﻿using OM;
using OM.Prodotti;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ProdottoManager : BaseManager<BaseProdotto>
    {
        public ProdottoManager() : base()
        {

        }

        public List<NomeIdentifierTipo> GetNomiProdotti()
        {
            return _repo.Query<BaseProdotto>().Select(x => new NomeIdentifierTipo()
            {
                Nome = x.Nome,
                Identifier = x.Identifier,
                Tipo = x.TipoProdotto
            })
            .OrderBy(x => x.Nome)
            .ToList()
            .GroupBy(x => x.Nome)
            .Select(x => x.First())
            .ToList();
        }

        public BaseProdotto SelectProdotto(string nomeProdotto, Guid fornitoreIdentifier, List<KeyValuePair<string, string>> proprieta)
        {
            return _repo.Query<BaseProdotto>()
                .Where(x => x.Nome == nomeProdotto && x.FornitoreIdentifier == fornitoreIdentifier)
                .Where(x => x.Proprieta == proprieta).SingleOrDefault();
        }

        public List<KeyValuePair<string, string>> GetProprietaByNomeProdottoAndFornitore(string nomeProdotto, Guid fornitoreIdentifier)
        {
            return _repo.Query<BaseProdotto>()
                .Where(x => x.Nome == nomeProdotto && x.FornitoreIdentifier == fornitoreIdentifier)
                .SelectMany(x => x.Proprieta)
                .ToList();
        }

        public IQueryable<string> GetAllProprietaNames()
        {
            return _repo.Query<BaseProdotto>()
                .SelectMany(x => x.Proprieta)
                .Select(x => x.Key)
                .Distinct();
        }

        public List<NomeIdentifier> GetFornitoriByNomeProdotto(string nomeProdotto)
        {
            return _repo.Query<BaseProdotto>()
                .Where(x => x.Nome.Contains(nomeProdotto))
                .Select(x => new NomeIdentifier
                {
                    Nome = x.Fornitore,
                    Identifier = x.FornitoreIdentifier
                })
                .Distinct()
                .ToList();
        }

        public BaseProdotto GetByName(string name)
        {
            return _repo.Query<BaseProdotto>().Where(x => x.Nome == name).SingleOrDefault();
        }
    }
}
