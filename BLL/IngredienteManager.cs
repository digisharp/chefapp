﻿using DAL;
using OM;
using OM.Prodotti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class IngredienteManager : BaseManager<BaseProdotto>
    {
        public IngredienteManager() : base()
        {
        }
        public IngredienteManager(MongoDbRepository mongo) : base(mongo)
        {
        }
        public new IQueryable<Ingrediente> GetAll()
        {
            return _repo.Query<BaseProdotto>().OfType<Ingrediente>();
        }
        public Ingrediente GetByName(string name,string fornitore)
        {
            return (Ingrediente)_repo.Query<BaseProdotto>().Where(x => x.Nome == name && x.Fornitore == fornitore).SingleOrDefault();
        }
        //public void Create(Ingrediente item)
        //{
        //    item.CreatedOn = DateTime.Now;
        //    item.CreatedBy = OwinUtility.GetUserIdentifier();
        //    _repo.Save<BaseProdotto>(item);
        //}
        //public void Update(Ingrediente item)
        //{
        //    item.ModifiedOn = DateTime.Now;
        //    item.ModifiedBy = OwinUtility.GetUserIdentifier();
        //    _repo.Save<BaseProdotto>(item);
        //}
        //public void Delete(Guid identifier)
        //{
        //    _repo.Delete<BaseProdotto>(x => x.Identifier == identifier);
        //}
    }
}
