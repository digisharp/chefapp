﻿using DAL;
using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class RicettaManager : BaseManager<Ricetta>
    {
        public RicettaManager() : base()
        {
        }
        public RicettaManager(MongoDbRepository mongo) : base(mongo)
        {
        }
        public IQueryable<Ricetta> GetRicette()
        {
            return _repo.Query<Ricetta>().Where(x => !x.IsBase);
        }
        public IQueryable<Ricetta> GetBasi()
        {
            return _repo.Query<Ricetta>().Where(x => x.IsBase);
        }
    }
}
