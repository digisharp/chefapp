﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BLL
{
    public static class OwinUtility
    {
        public static string GetUserNomeCompleto()
        {
            var principal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            return principal.Claims.Where(x => x.Type == ClaimTypes.Name).Select(x => x.Value).SingleOrDefault();
        }
        public static Guid? GetUserIdentifier()
        {
            var principal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var val = principal.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).SingleOrDefault();
            if (val != null)
                return new Guid(val);
            else
                return null;
        }
        public static string GetUserRole()
        {
            var principal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            return principal.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value).SingleOrDefault();
        }
    }
}
