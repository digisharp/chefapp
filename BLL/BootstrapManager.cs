﻿using OM.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class BootstrapManager
    {
        public static void Startup()
        {
            var userManager = new UserManager();
            var email = ConfigurationManager.AppSettings["adminEmail"];
            var admin = userManager.GetByEmail(email);
            if(admin == null)
            {
                admin = new Admin();
                var password = ConfigurationManager.AppSettings["adminPassword"];
                admin.Email = email;
                admin.Password = password;
                admin.Username = "Admin";
                admin.Role = OM.Enum.UserRole.Admin;
                userManager.Save(admin);
            }
        }
    }
}
