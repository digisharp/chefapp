﻿using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class CustomExtensions
    {
        //public static IQueryable<T> DistinctBy<T>(this IQueryable<T> input, string property) where T : class
        //{
        //    EqualityComparer<string> comparer = new EqualityCompare
        //    return input.Distinct(x => x.GerarchiaInterna.GerarchiaResponsabiliHistory.Any(y =>
        //            y.GerarchiaResponsabili.UtenteInternoIdentifier == utenteInternoIdentifier
        //            && y.MeseProduttivo == periodoProduttivo.Mese
        //            && y.AnnoProduttivo == periodoProduttivo.Anno));

        //    }

        //}

        public static TableResult<T> AsTableResult<T>(this IQueryable<T> input, int perPage = 0, int currentPage = 0)
        {
            int itemsTotal = input.Count();
            if(perPage == 0)
            {
                return new TableResult<T>()
                {
                    Data = input.ToList()
                };
            }

            int totalePages = (int)Math.Ceiling((double)itemsTotal / (double)perPage);
            var pagedObjs = input.Skip((currentPage - 1) * perPage).Take(perPage).ToList();
            return new TableResult<T>()
            {
                Total = itemsTotal,
                Current_page = currentPage,
                Per_page = perPage,
                Data = pagedObjs,
                Last_page = totalePages
            };
        }

    }
}
