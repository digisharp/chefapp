﻿using DAL;
using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BaseManager<T> where T : class, IBaseEntity
    {
        protected MongoDbRepository _repo;
        public BaseManager()
        {
            _repo = new MongoDbRepository();
        }
        public BaseManager(MongoDbRepository repo)
        {
            _repo = repo;
        }
        public IQueryable<T> GetAll()
        {
            return _repo.Query<T>();
        }
        public T GetById(Guid identifier)
        {
            return _repo.GetById<T>(identifier);
        }
        public Guid? Save(T item)
        {
            if (item.CreatedOn == DateTime.MinValue)
            {
                item.CreatedOn = DateTime.UtcNow;
                item.CreatedBy = OwinUtility.GetUserIdentifier();
            }
            item.ModifiedOn = DateTime.UtcNow;
            item.ModifiedBy = OwinUtility.GetUserIdentifier();
            return _repo.Save<T>(item);
        }
        public void Delete(Guid identifier)
        {
            _repo.Delete<T>(x => x.Identifier == identifier);
        }
    }
}
