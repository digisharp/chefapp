﻿using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class StoricoProduzioneManager : BaseManager<StoricoProduzione>
    {
        public StoricoProduzioneManager() : base()
        {

        }
        public IQueryable<StoricoProduzione> GetByUserIdentifier(Guid userIdentifier)
        {
            return _repo.Query<StoricoProduzione>().Where(x => x.UserIdentifier == userIdentifier);
        }
    }
}
