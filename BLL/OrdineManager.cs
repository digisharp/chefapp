﻿using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class OrdineManager : BaseManager<Ordine>
    {
        public OrdineManager() : base()
        {
            
        }
        public void SendMailToFornitori(OM.Ordine ordine)
        {
            var prodottiPerFornitore = ordine.Prodotti.GroupBy(x => x.FornitoreIdentifier);
            var userManager = new UserManager();
            var fornitoreManager = new FornitoreManager();
            foreach (var fornitoreGroup in prodottiPerFornitore)
            {
                var fornitore = fornitoreManager.GetById(fornitoreGroup.Key);
                MailingManager.SendOrdineToFornitore(
                    ordine.NomeCliente, 
                    new MailAddress(fornitore.Email), fornitoreGroup.ToList());
            }
        }
    }
}
