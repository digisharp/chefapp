﻿using OM;
using OM.Enum;
using OM.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserManager : BaseManager<BaseUser>
    {
        public UserManager() : base()
        {

        }

        public IQueryable<FornitoreOld> GetFornitori()
        {
            return _repo.Query<BaseUser>().Where(x => x.Role == UserRole.Fornitore).OfType<FornitoreOld>();
        }

        public BaseUser GetByEmailAndPassword(string email, string password) {
            return _repo.Query<BaseUser>().Where(x => x.Email.ToLower() == email.ToLower() && x.Password == password).SingleOrDefault();
        }

        public BaseUser GetByEmail(string email)
        {
            return _repo.Query<BaseUser>().Where(x => x.Email.ToLower() == email.ToLower()).SingleOrDefault();
        }

        public void ToggleRicettaPreferita(Guid userIdentifier, Guid ricettaIdentifier)
        {
            var user = (Cliente)GetById(userIdentifier);
            if (user.RicettePreferite.Contains(ricettaIdentifier))
                user.RicettePreferite.Remove(ricettaIdentifier);
            else
                user.RicettePreferite.Add(ricettaIdentifier);
            Save(user);
        }
    }
}
