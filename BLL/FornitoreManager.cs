﻿using DAL;
using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class FornitoreManager : BaseManager<Fornitore>
    {
        public FornitoreManager() : base()
        {
        }
        public FornitoreManager(MongoDbRepository mongo) : base(mongo)
        {

        }
        public Fornitore GetByRagioneSociale(string ragioneSociale)
        {
            return _repo.Query<Fornitore>().Where(x => x.RagioneSociale == ragioneSociale).SingleOrDefault();
        }
    }
}
