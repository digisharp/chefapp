﻿using DAL;
using OfficeOpenXml;
using OM;
using OM.Prodotti;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace BLL
{
    public class ConfigManager : BaseManager<Config>
    {
        public Config GetConfig()
        {
            return GetAll().FirstOrDefault();
        }

        public void ReadIngredientiInterni(Stream fileStream)
        {
            var mongo = new MongoDbRepository();
            var ingredientiManager = new IngredienteManager(mongo);
            var fornitoriManager = new FornitoreManager(mongo);
            using (ExcelPackage package = new ExcelPackage(fileStream))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                for (int row = 2; row <= worksheet.Dimension.Rows; row++)
                {
                    var nomeIngrediente = (string)worksheet.Cells[row, 5].Value;
                    var fornitore = (string)worksheet.Cells[row, 3].Value;
                    // TODO 0 LT: Da concludere. Verificare qual'è la chiave naturale per capire se il prodotto è già inserito
                    Ingrediente ingrediente = ingredientiManager.GetByName(nomeIngrediente, fornitore);
                    if (ReferenceEquals(ingrediente, null))
                        ingrediente = new Ingrediente();


                    ingrediente.Fornitore = fornitore;
                    var existingFornitore = fornitoriManager.GetByRagioneSociale(ingrediente.Fornitore);
                    if (existingFornitore != null)
                    {
                        ingrediente.FornitoreIdentifier = existingFornitore.Identifier;
                    }
                    else
                    {
                        var newFornitore = new Fornitore();
                        newFornitore.RagioneSociale = ingrediente.Fornitore;
                        fornitoriManager.Save(newFornitore);
                        ingrediente.FornitoreIdentifier = newFornitore.Identifier;
                    }

                    ingrediente.Categoria = (string)worksheet.Cells[row, 4].Value;
                    ingrediente.Nome = (string)worksheet.Cells[row, 5].Value;
                    ingrediente.PrezzoDiListino = decimal.Parse(worksheet.Cells[row, 6].Value.ToString());
                    ingrediente.Dettaglio.Acqua = decimal.Parse(worksheet.Cells[row, 7].Value.ToString());
                    ingrediente.Dettaglio.Lipidi = decimal.Parse(worksheet.Cells[row, 8].Value.ToString());
                    ingrediente.Dettaglio.Proteine = decimal.Parse(worksheet.Cells[row, 9].Value.ToString());
                    ingrediente.Dettaglio.Zuccheri = decimal.Parse(worksheet.Cells[row, 11].Value.ToString());
                    ingrediente.Dettaglio.Fibre = decimal.Parse(worksheet.Cells[row, 12].Value.ToString());
                    ingrediente.Dettaglio.Carboidrati = decimal.Parse(worksheet.Cells[row, 13].Value.ToString());
                    ingrediente.Dettaglio.Ceneri = decimal.Parse(worksheet.Cells[row, 14].Value.ToString());
                    ingrediente.Dettaglio.AltriSolidi = decimal.Parse(worksheet.Cells[row, 15].Value.ToString());
                    ingrediente.Dettaglio.Pac = decimal.Parse(worksheet.Cells[row, 16].Value.ToString());
                    ingrediente.Dettaglio.Pod = decimal.Parse(worksheet.Cells[row, 17].Value.ToString());
                    ingrediente.Dettaglio.Kcal = decimal.Parse(worksheet.Cells[row, 18].Value.ToString());
                    ingrediente.Dettaglio.Kjoul = decimal.Parse(worksheet.Cells[row, 19].Value.ToString());

                    ingredientiManager.Save(ingrediente);
                }
            }
        }

        public void ReadIngredientiCsv(Stream inputStream, bool skipHeader)
        {
            var ingredientiManager = new IngredienteManager();
            using (var reader = new StreamReader(inputStream))
            {
                var lineCounter = 0;
                bool skipFirst = skipHeader;
                while (!reader.EndOfStream)
                {
                    lineCounter++;
                    try
                    {
                        var line = reader.ReadLine();
                        if (skipFirst)
                        {
                            skipFirst = false;
                            continue;
                        }
                        var values = line.Split(';');
                        var newIngrediente = new Ingrediente();
                        //newIngrediente.Dettaglio.CodiceProdotto = values[0];
                        //newIngrediente.Dettaglio.ClasseIngrediente = values[2];
                        //newIngrediente.Nome = values[3];
                        ////ingrediente.quanti QUANTITA
                        //newIngrediente.Dettaglio.Umidita = decimal.Parse(values[6], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.Grassi = decimal.Parse(values[7], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.Proteine = decimal.Parse(values[8], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.Glucidi = decimal.Parse(values[9], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.Fibra = decimal.Parse(values[10], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.Carboidrati = decimal.Parse(values[11], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.Ceneri = decimal.Parse(values[12], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.AltriSolidi = decimal.Parse(values[13], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.SolidiTotali = decimal.Parse(values[14], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Pac = decimal.Parse(values[15], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Pod = decimal.Parse(values[16], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.KiloCalorie = decimal.Parse(values[17], new CultureInfo("it-IT"));
                        //newIngrediente.Dettaglio.Nutrienti.KiloJoules = decimal.Parse(values[18], new CultureInfo("it-IT"));

                        var existing = ingredientiManager.GetByName(newIngrediente.Nome, newIngrediente.Fornitore);
                        if (existing != null)
                            newIngrediente.Identifier = existing.Identifier;

                        ingredientiManager.Save(newIngrediente);
                    }
                    catch (Exception ex)
                    {
                        var pippo = 2;
                    }
                }
            }
        }

        public string WriteIngredientiInterni()
        {
            var mongo = new MongoDbRepository();
            var ingredientiManager = new IngredienteManager(mongo);
            using (ExcelPackage excel = new ExcelPackage())
            {
                // get the first worksheet in the workbook
                var worksheet = excel.Workbook.Worksheets.Add("Worksheet1");
                var headerRow = new List<string[]>()
                {
                    new string[] { "CODICE INTERNO"
                        ,"CODICE FORNITORE"
                        ,"FORNITORE"
                        ,"CATEGORIA"
                        ,"NOME"
                        ,"COSTO"
                        ,"UMIDITA' - ACQUA"
                        ,"LIPIDI - GRASSI"
                        ,"PROTEINE"
                        ,"SOLIDI MAGRI DEL LATTE - SOLIDI NON GRASSI DEL LATTE"
                        ,"ZUCCHERI"
                        ,"FIBRE"
                        ,"CARBOIDRATI"
                        ,"CENERI"
                        ,"ALTRI SOLIDI"
                        ,"PAC"
                        ,"POD"
                        ,"Kcal"
                        ,"Kjoul" }
                };

                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                var i = 2;
                foreach (var ingrediente in ingredientiManager.GetAll())
                {
                    worksheet.Cells[i, 3].Value = ingrediente.Fornitore;
                    worksheet.Cells[i, 4].Value = ingrediente.Categoria;
                    worksheet.Cells[i, 5].Value = ingrediente.Nome;
                    worksheet.Cells[i, 6].Value = ingrediente.PrezzoDiListino;
                    worksheet.Cells[i, 7].Value = ingrediente.Dettaglio.Acqua;
                    worksheet.Cells[i, 8].Value = ingrediente.Dettaglio.Lipidi;
                    worksheet.Cells[i, 9].Value = ingrediente.Dettaglio.Proteine;
                    worksheet.Cells[i, 10].Value = ingrediente.Dettaglio.SolidiTotali;
                    worksheet.Cells[i, 11].Value = ingrediente.Dettaglio.Zuccheri;
                    worksheet.Cells[i, 12].Value = ingrediente.Dettaglio.Fibre;
                    worksheet.Cells[i, 13].Value = ingrediente.Dettaglio.Carboidrati;
                    worksheet.Cells[i, 14].Value = ingrediente.Dettaglio.Ceneri;
                    worksheet.Cells[i, 15].Value = ingrediente.Dettaglio.AltriSolidi;
                    worksheet.Cells[i, 16].Value = ingrediente.Dettaglio.Pac;
                    worksheet.Cells[i, 17].Value = ingrediente.Dettaglio.Pod;
                    worksheet.Cells[i, 18].Value = ingrediente.Dettaglio.Kcal;
                    worksheet.Cells[i, 19].Value = ingrediente.Dettaglio.Kjoul;

                    i += 1;
                }

                var basePath = HostingEnvironment.MapPath("~/media/");
                FileInfo excelFile = new FileInfo(string.Concat(basePath, "\\ingredienti.xlsx"));

                excel.SaveAs(excelFile);

                return excelFile.FullName;
            }

        }
    }

}
