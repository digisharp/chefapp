﻿using Mustache;
using OM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace BLL
{

    public static class MailingManager
    {
        public class InternalMailAddress
        {
            public const string Debug = "pietro.manneschi@gmail.com";
            public const string NoReply = "no-reply@automatika.tech";
            public const string Info = "info@automatika.tech";
            public const string Amministrazione = "info@automatika.tech";

            public static readonly MailAddress DefaultSender = new MailAddress(NoReply, "Gelato!");
            public static readonly MailAddress InfoMailAddress = new MailAddress(Info, "Gelato!");
            public static readonly MailAddress NoReplyMailAddress = new MailAddress(NoReply, "Gelato!");
        }
        public class MailTemplate
        {
            public const string GenericMail = "GenericMail.html";
        }
        public static void SendOrdineToFornitore(string nomeCliente, MailAddress mailFornitore, List<ProdottoOrdine> prodotti)
        {
            var title = "Ordine in arrivo da " + nomeCliente;

            var body = "<h1>" + title + "</h1>"
                + "<table><tr><td>Nome</td><td>Prezzo</td><td>Quantità</td><td></td></tr>";

            foreach(var prod in prodotti)
            {
                body += "<tr>";
                body += "<td>" + prod.NomeProdotto + "</td>";
                body += "<td>" + prod.Prezzo + "</td>";
                body += "<td>" + prod.Quantita + "</td>";
                body += "<td>" + prod.UnitaDiMisura + "</td>";
                body += "</tr>";
            }

            body += "</tr></table>";

            var subject = "Gelato - " + title;
            body = GenerateBodyFromTemplate(MailTemplate.GenericMail, new { title = title, message = body });
            
            SendEmail(subject, body, mailFornitore, InternalMailAddress.NoReplyMailAddress);
        }
        public static void PasswordResetEmail(MailAddress to, string resetLink)
        {
            var title = "Reset Password";
            var clickHere = "Click here to reset your password";

            var body = "<h1>" + title + " HumanForLife</h1>"
                + "<a href='" + resetLink + "'<h3>" + clickHere + "</h3></a>";
            var subject = "HumanForLife - " + title;

            //SendEmail(to, OM.Const.InternalMailAddress.NoReply, body, subject);
            SendEmail(subject, body, to, InternalMailAddress.NoReplyMailAddress);
        }
        public static void StartModificaPassword(string link, string email)
        {
            var title = "Password reset";
            var subject = "HumanForLife - " + title;
            var message = "This is an automated message for password recovery." + Environment.NewLine +
                "If you haven't requested this operation, you can delete this email. Click the link below to complete the process.";

            var obj = new { message, title, link };
            var body = GenerateBodyFromTemplate(MailTemplate.GenericMail, obj);

            SendEmail(subject, body, new MailAddress(email), InternalMailAddress.NoReplyMailAddress);
        }

        #region Send Mail Method 

        private static void SendEmail(string subject, string body, MailAddress to)
        {
            var toList = new Collection<MailAddress>();
            toList.Add(to);
            SendEmail(subject, body, toList, null);
        }
        private static void SendEmail(string subject, string body, MailAddress to, MailAddress replyTo)
        {
            var toList = new Collection<MailAddress>();
            toList.Add(to);
            var replyToList = new Collection<MailAddress>();
            replyToList.Add(replyTo);
            SendEmail(subject, body, toList, replyToList);
        }
        private static void SendEmail(string subject, string body, IEnumerable<MailAddress> toList)
        {
            SendEmail(subject, body, toList, null);
        }
        private static void SendEmail(string subject, string body, IEnumerable<MailAddress> toList, IEnumerable<MailAddress> replyToList)
        {
            var mailMessage = new MailMessage();
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.From = InternalMailAddress.DefaultSender;
#if DEBUG
            mailMessage.To.Add(new MailAddress(InternalMailAddress.Debug));
            mailMessage.ReplyToList.Add(new MailAddress(InternalMailAddress.Debug));
            mailMessage.Subject = "[DEBUG] " + subject;
#else
            foreach (var item in toList)
            {
                mailMessage.To.Add(item);
            }
            foreach (var item in replyToList)
            {
                mailMessage.ReplyToList.Add(item);
            }
#endif
            SmtpClientSend(mailMessage);
        }
        private static void SendEmail(string subject, string body, IEnumerable<MailAddress> toList, IEnumerable<MailAddress> replyToList,
            StreamMailAttachment attachment)
        {
            var attachmentList = new List<StreamMailAttachment>();
            attachmentList.Add(attachment);
            SendEmail(subject, body, toList, replyToList, attachmentList);
        }
        private static void SendEmail(string subject, string body, IEnumerable<MailAddress> toList, IEnumerable<MailAddress> replyToList,
            IEnumerable<StreamMailAttachment> attachments)
        {
            var mailMessage = new MailMessage();
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.From = InternalMailAddress.NoReplyMailAddress;


#if DEBUG
            mailMessage.To.Add(new MailAddress(InternalMailAddress.Debug));
            mailMessage.ReplyToList.Add(new MailAddress(InternalMailAddress.Debug));
            mailMessage.Subject = "[DEBUG] " + subject;

#else
            foreach (var item in toList)
            {
                mailMessage.To.Add(item);
            }
            foreach (var item in replyToList)
            {
                mailMessage.ReplyToList.Add(item);
            }
           
#endif

            foreach (StreamMailAttachment attachment in attachments)
            {
                attachment.Stream.Position = 0;
                Attachment data = new Attachment(attachment.Stream, attachment.NomeFile, attachment.MimeType);
                //data.TransferEncoding = TransferEncoding.Base64;
                //var attach = new Attachment(attachment.ContentStream, attachment.NomeFile, attachment.MIMEType);
                mailMessage.Attachments.Add(data);

            }
            SmtpClientSend(mailMessage);
        }
        #endregion

        private static void SmtpClientSend(MailMessage mailMessage)
        {
            var smtpHost = ConfigurationManager.AppSettings["smtpHost"];
            var smtpPort = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);
            var smtpClient = new SmtpClient(smtpHost, smtpPort);
            smtpClient.Port = smtpPort;
            smtpClient.EnableSsl = true;
            var user = ConfigurationManager.AppSettings["smtpUser"];
            var password = ConfigurationManager.AppSettings["smtpPassword"];
            smtpClient.Credentials = new NetworkCredential(user, password);
            smtpClient.Send(mailMessage);

        }
        private static string GenerateBodyFromTemplate(string templateName, dynamic item)
        {
            var templatePath = HostingEnvironment.MapPath("~");
            if (templatePath == null)
                templatePath = ".";
            var template = File.ReadAllText(templatePath + @"\MailTemplate\" + templateName);
            FormatCompiler compiler = new FormatCompiler();
            Generator generator = compiler.Compile(template);
            string result = generator.Render(item);
            return result;
        }
    }
}
