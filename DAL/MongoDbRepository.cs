﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using OM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{

    public class MongoDbRepository
    {
        public static bool AlreadyRegistered { get; set; }
        private MongoClient _mongoClient = null;
        public IMongoDatabase _database;
        public MongoDbRepository()
        {
            _mongoClient = new MongoClient(ConfigurationManager.AppSettings["mongoDbConnectionString"]);
            if (!AlreadyRegistered)
            {
                MongoMapping.RegisterMapping();
                BsonSerializer.RegisterSerializer(typeof(decimal), new DecimalSerializer(BsonType.Decimal128));
                BsonSerializer.RegisterSerializer(typeof(decimal?), new NullableSerializer<decimal>(new DecimalSerializer(BsonType.Decimal128)));
                AlreadyRegistered = true;
            }
            _database = _mongoClient.GetDatabase(ConfigurationManager.AppSettings["mongoDbDataBaseName"]);
        }
        public bool Any<T>(System.Linq.Expressions.Expression<Func<T, bool>> expression) where T : class, IBaseEntity
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            return collection.AsQueryable().Any(expression);
        }
        public void Delete<T>(System.Linq.Expressions.Expression<Func<T, bool>> expression) where T : class, IBaseEntity
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            collection.DeleteManyAsync(expression).Wait();
        }
        public void Delete<T>(T item) where T : class, IBaseEntity
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            collection.DeleteOneAsync(x => x.Identifier == item.Identifier).Wait();
        }
        public void DeleteAll<T>() where T : class, IBaseEntity
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            _database.DropCollectionAsync(collection.CollectionNamespace.CollectionName).Wait();
        }
        public IQueryable<T> Query<T>() where T : class
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            return collection.AsQueryable();
        }
        public Guid? Save<T>(T item) where T : class, IBaseEntity
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            Guid? result = null;
            if (item.Identifier == Guid.Empty)
            {
                collection.InsertOne(item);
                result = item.Identifier;
            }
            else
            {
                var ris = collection.ReplaceOne<T>(
                    x => x.Identifier == item.Identifier,
                    item,
                    new UpdateOptions { IsUpsert = true });
                if (ris.UpsertedId != null)
                    result = ris.UpsertedId.AsNullableGuid;
            }
            return result;
        }
        public T GetById<T>(Guid identifier) where T : class, IBaseEntity
        {
            var collection = _database.GetCollection<T>(typeof(T).Name);
            return collection.AsQueryable().SingleOrDefault(t => t.Identifier == identifier);
        }
    }


}
