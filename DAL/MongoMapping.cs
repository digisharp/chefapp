﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using OM;
using OM.Prodotti;
using OM.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MongoMapping
    {
        public static void RegisterMapping()
        {
            BsonClassMap.RegisterClassMap<BaseEntity>(x =>
            {
                x.AutoMap();
                x.SetIdMember(x.GetMemberMap(c => c.Identifier));
                x.IdMemberMap.SetIdGenerator(CombGuidGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<BaseProdotto>(x =>
            {
                x.AutoMap();
                //x.UnmapField(c => c.Identifier);
                x.UnmapField(c => c.Dettaglio);
                x.MapField(c => c.TipoProdotto);
            });

            BsonClassMap.RegisterClassMap<Ingrediente>(x =>
            {
                x.AutoMap();
            });
            BsonClassMap.RegisterClassMap<Articolo>(x =>
            {
                x.AutoMap();
            });
            BsonClassMap.RegisterClassMap<DettaglioIngrediente>(x =>
            {
                x.AutoMap();
                x.MapField(c => c.SolidiTotali);
            });


            BsonClassMap.RegisterClassMap<BaseUser>(x =>
            {
                x.AutoMap();
                //x.UnmapField(c => c.Identifier);
            });

            BsonClassMap.RegisterClassMap<Cliente>(x =>
            {
                x.AutoMap();
            });
            BsonClassMap.RegisterClassMap<Operatore>(x =>
            {
                x.AutoMap();
            });
            BsonClassMap.RegisterClassMap<FornitoreOld>(x =>
            {
                x.AutoMap();
            });
            BsonClassMap.RegisterClassMap<Admin>(x =>
            {
                x.AutoMap();
            });
        }
    }
}
