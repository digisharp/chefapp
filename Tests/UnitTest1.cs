﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BLL;
using OM;
using System.Collections.Generic;
using OM.Prodotti;
using System.Linq;
using Newtonsoft.Json;
using OM.Users;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestOperatoreSave()
        {
            var mgr = new UserManager();

            var operatore = new Operatore();
            operatore.Nome = "Giuseppe";
            operatore.Cognome = "Scendiscale";

        }
        [TestMethod]
        public void TestClienteSave()
        {
            var mgr = new UserManager();

            var cliente = new Cliente();
            cliente.RagioneSociale = "Corbetta 12";
            cliente.PartitaIva = "0902321931";
            cliente.Referente = "Antonio";
        }
        [TestMethod]
        public void TestTranslation()
        {
            var mgr = new ConfigManager();

            var config = new Config();

            var newDict = new Dictionary<string, string>();
            newDict.Add("italiano", "Gelaterie!!");
            newDict.Add("inglese", "Ice creams!!");
            config.Translations.Add("menu_title", newDict);

        }

        [TestMethod]
        public void TestBaseProdottoSave()
        {
            var mgr = new ProdottoManager();

            var ingrediente = new Ingrediente();
            ingrediente.Nome = "Latte in polvere";
            ingrediente.Proprieta.Add(new KeyValuePair<string, string>("Formato", "Sacco 1Kg"));

            ingrediente.PrezzoDiListino = 10;
            var ingrediente2 = new Ingrediente();
            ingrediente2.Nome = "Latte in polvere";
            ingrediente2.Proprieta.Add(new KeyValuePair<string, string>("Formato", "Sacco 2Kg"));
            ingrediente2.PrezzoDiListino = 20;
            var ingrediente3 = new Ingrediente();
            ingrediente3.Nome = "Latte in polvere";
            ingrediente3.Proprieta.Add(new KeyValuePair<string, string>("Formato", "Sacco 5Kg"));
            ingrediente3.PrezzoDiListino = 48;


            var attrezzatura = new Articolo();
            attrezzatura.Nome = "Macchina del gelato";

            var pippo = mgr.GetAll().First();
            var json = JsonConvert.SerializeObject(pippo);
        }
    }
}
