﻿using BLL;
using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using OM.Enum;

namespace Gelaterie.Controllers
{
    [Authorize]
    public class OrdineController : ApiController
    {
        [HttpGet]
        [Route("Ordine/GetAll")]
        public IHttpActionResult GetAll()
        {
            var manager = new OrdineManager();

            var ris = manager.GetAll().ToList();
            return Ok(ris);
        }

        [HttpGet]
        [Route("Ordine/GetAllFilteredBy")]
        public IHttpActionResult GetAllFilteredBy(Guid identifier, string role)
        {
            IEnumerable<Ordine> ordini = new List<Ordine>();
            var manager = new OrdineManager();
            if (!ReferenceEquals(null, identifier) && role == UserRole.Fornitore)
            {
                ordini = manager.GetAll()
                    .Where(o => o.Prodotti.Any(d => d.FornitoreIdentifier == identifier));
                //.Select(o => new { Order = o, Details = o.Detail.Where(d => d.StockCode == "STK2") });
            }
            else
            {
                ordini = manager.GetAll();
            }

            var ris = ordini.ToList();
            return Ok(ris);
        }

        [HttpGet]
        [Route("Ordine/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var manager = new OrdineManager();
            var ris = manager.GetById(identifier);
            return Ok(ris);
        }

        [HttpPost]
        [Route("Ordine/Create")]
        public IHttpActionResult Create(Ordine item)
        {
            item.ClienteIdentifier = OwinUtility.GetUserIdentifier().Value;
            item.NomeCliente = OwinUtility.GetUserNomeCompleto();
            var manager = new OrdineManager();
            var lastOrder = manager.GetAll().OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (lastOrder == null)
                item.CodiceOrdine = "1";
            else
                item.CodiceOrdine = (long.Parse(lastOrder.CodiceOrdine) + 1).ToString();

            manager.Save(item);
            manager.SendMailToFornitori(item);
            return Ok();
        }

        [HttpGet]
        [Route("Ordine/Delete")]
        public IHttpActionResult Delete(Guid identifier)
        {
            var manager = new OrdineManager();
            manager.Delete(identifier);
            return Ok();
        }
    }
}