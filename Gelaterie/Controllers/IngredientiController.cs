﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BLL;
using OM;
using OM.Viewmodels;
using OM.Prodotti;
using MoreLinq;

namespace Gelaterie.Controllers
{
    public class IngredientiController : ApiController
    {
        [HttpGet]
        [Route("Ingrediente/GetAll")]
        public IHttpActionResult GetAll()
        {
            var manager = new IngredienteManager();

            var ris = manager.GetAll()
                .Where(x => x.TipoProdotto == OM.Enum.TipoProdotto.Ingrediente)
                .Select(x => new IngredienteVm()
                {
                    Identifier = x.Identifier,
                    Nome = x.Nome,//string.Format("{0} - [{1}]",x.Nome, x.Fornitore),
                    Fornitore = x.Fornitore,
                    PrezzoDiListino = x.PrezzoDiListino,
                    QuantitaMillesimale = x.QuantitaMillesimale,
                    NomeIngredienteFornitore = x.Nome + " [" + x.Fornitore+ "]"
                })
                .DistinctBy(x => x.Nome)
                .ToList();

            return Ok(ris);
        }

        [HttpGet]
        [Route("Ingrediente/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var manager = new IngredienteManager();

            var ris = manager.GetById(identifier);

            return Ok(ris);
        }

        [HttpPost]
        [Route("Ingrediente/Save")]
        public IHttpActionResult Update(Ingrediente item)
        {
            item.Nome = item.Nome.ToUpper();
            for(var i = 0; i < item.Proprieta.Count; i++)
            {
                item.Proprieta[i] = new KeyValuePair<string, string>(item.Proprieta[i].Key.ToUpper(), item.Proprieta[i].Value.ToUpper());
            }
            var manager = new ProdottoManager();
            manager.Save(item);
            return Ok();
        }

        [HttpGet]
        [Route("Ingrediente/Delete")]
        public IHttpActionResult Delete(Guid identifier)
        {
            var manager = new IngredienteManager();
            manager.Delete(identifier);
            return Ok();
        }
    }
}