﻿using BLL;
using OM;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Gelaterie.Controllers
{
    public class FornitoriController : ApiController
    {
        [HttpGet]
        [Route("Fornitori/GetAll")]
        public IHttpActionResult GetAll()
        {
            var fornitoreManager = new FornitoreManager();
            var ris = fornitoreManager.GetAll().OrderBy( x => x.RagioneSociale ).ToList();
            return Ok(ris);
        }

        [HttpGet]
        [Route("Fornitori/GetNomeIdentifier")]
        public IHttpActionResult GetNomeIdentifier()
        {
            var fornitoreManager = new FornitoreManager();
            var ris = fornitoreManager.GetAll().Select(x => new
            {
                x.Identifier,
                x.RagioneSociale
            })
            .OrderBy( x => x.RagioneSociale)
            .ToList()
            .Select(x => new NomeIdentifier()
            {
                Identifier = x.Identifier,
                Nome = x.RagioneSociale
            });
            return Ok(ris);
        }

        [HttpGet]
        [Route("Fornitori/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var fornitoreManager = new FornitoreManager();
            var ris = fornitoreManager.GetById(identifier);
            return Ok(ris);
        }

        [HttpPost]
        [Route("Fornitori/Save")]
        public IHttpActionResult Save(Fornitore fornitore)
        {
            var fornitoreManager = new FornitoreManager();
            fornitoreManager.Save(fornitore);
            return Ok();
        }
    }
}