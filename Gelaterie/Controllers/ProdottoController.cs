﻿using BLL;
using OM.Prodotti;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Gelaterie.Controllers
{
    [Authorize]
    public class ProdottoController : ApiController
    {
        [HttpGet]
        [Route("Prodotto/GetAll")]
        public IHttpActionResult GetAll(int perPage = 0, int currentPage = 0)
        {
            var manager = new ProdottoManager();
            var ris = manager.GetAll().Where(x => x.TipoProdotto != OM.Enum.TipoProdotto.Ingrediente).AsTableResult(perPage, currentPage);
            return Ok(ris);
        }

        [HttpGet]
        [Route("Prodotto/GetNomiProdotti")]
        public IHttpActionResult GetNomiProdotti()
        {
            var manager = new ProdottoManager();
            var ris = manager.GetNomiProdotti();
            return Ok(ris);
        }

        [HttpGet]
        [Route("Prodotto/GetFornitoriForOrdine")]
        public IHttpActionResult GetFornitoriForOrdine(string nomeProdotto)
        {
            var manager = new ProdottoManager();
            var ris = manager.GetFornitoriByNomeProdotto(nomeProdotto);
            return Ok(ris);
        }

        [HttpGet]
        [Route("Prodotto/GetProprietaForOrdine")]
        public IHttpActionResult GetProprietaForOrdine(string nomeProdotto, Guid fornitoreIdentifier)
        {
            var manager = new ProdottoManager();
            var ris = manager.GetProprietaByNomeProdottoAndFornitore(nomeProdotto, fornitoreIdentifier);
            return Ok(ris);
        }

        [HttpGet]
        [Route("Prodotto/GetAllProprietaNames")]
        public IHttpActionResult GetAllProprietaNames()
        {
            var manager = new ProdottoManager();
            var ris = manager.GetAllProprietaNames().ToList();
            return Ok(ris);
        }

        [HttpGet]
        [Route("Prodotto/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var manager = new ProdottoManager();
            var ris = manager.GetById(identifier);
            return Ok(ris);
        }

        [HttpPost]
        [Route("Prodotto/Update")]
        public IHttpActionResult Update(BaseProdotto item)
        {
            item.Nome = item.Nome.ToUpper();
            var manager = new ProdottoManager();
            manager.Save(item);
            return Ok();
        }

        [HttpPost]
        [Route("Prodotto/Create")]
        public IHttpActionResult Create(NewProdottoVm item)
        {
            item.Nome = item.Nome.ToUpper();
            var manager = new ProdottoManager();
            Guid? ris = null;
            if (item.TipoProdotto == OM.Enum.TipoProdotto.Articolo)
            {
                var toSave = new Articolo(item);
                ris = manager.Save(toSave);
            }
            else if (item.TipoProdotto == OM.Enum.TipoProdotto.Ingrediente)
            {
                var toSave = new Ingrediente(item);
                ris = manager.Save(toSave);
            }
            return Ok(ris);
        }

        [HttpPost]
        [Route("Prodotto/SelectProdotto")]
        public IHttpActionResult SelectProdotto(SelectProdottoVm vm)
        {
            var manager = new ProdottoManager();
            var ris = manager.SelectProdotto(vm.Nome, vm.FornitoreIdentifier, vm.Proprieta);
            return Ok(ris);
        }

        [HttpGet]
        [Route("Prodotto/Delete")]
        public IHttpActionResult Delete(Guid identifier)
        {
            var manager = new ProdottoManager();
            manager.Delete(identifier);
            return Ok();
        }
    }
}