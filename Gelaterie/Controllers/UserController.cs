﻿using BLL;
using OM;
using OM.Enum;
using OM.Users;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Gelaterie.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        [Route("User/GetAll")]
        public IHttpActionResult GetAll()
        {
            var manager = new UserManager();

            var ris = manager.GetAll().ToList();

            return Ok(ris);
        }

        [HttpGet]
        [Route("User/GetFornitori")]
        public IHttpActionResult GetFornitori()
        {
            var manager = new UserManager();

            var ris = manager.GetFornitori().ToList();

            return Ok(ris);
        }

        [HttpGet]
        [Route("User/GetFornitoriForSelect")]
        public IHttpActionResult GetFornitoriForSelect()
        {
            var manager = new UserManager();

            var ris = manager.GetFornitori().Select(x => new NomeIdentifier()
            {
                Identifier = x.Identifier,
                Nome = x.RagioneSociale
            }).ToList();

            return Ok(ris);
        }

        [HttpGet]
        [Route("User/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var manager = new UserManager();

            var ris = manager.GetById(identifier);

            return Ok(ris);
        }

        [HttpPost]
        [Route("User/SaveCliente")]
        public IHttpActionResult SaveCliente(Cliente item)
        {
            var manager = new UserManager();
            var existing = manager.GetByEmail(item.Email);
            if(existing != null && item.Identifier != existing.Identifier)
                return BadRequest("Email già usata");
            manager.Save(item);
            return Ok();
        }

        [HttpPost]
        [Route("User/SaveFornitore")]
        public IHttpActionResult SaveFornitore(FornitoreOld item)
        {
            var manager = new UserManager();
            var existing = manager.GetByEmail(item.Email);
            if (existing != null && item.Identifier != existing.Identifier)
                return BadRequest("Email già usata");
            manager.Save(item);
            return Ok();
        }

        [HttpPost]
        [Route("User/SaveAdmin")]
        public IHttpActionResult SaveAdmin(Admin item)
        {
            var manager = new UserManager();
            var existing = manager.GetByEmail(item.Email);
            if (existing != null && item.Identifier != existing.Identifier)
                return BadRequest("Email già usata");
            manager.Save(item);
            return Ok();
        }

        [HttpPost]
        [Route("User/SaveOperatore")]
        public IHttpActionResult SaveOperatore(Operatore item)
        {
            var manager = new UserManager();
            var existing = manager.GetByEmail(item.Email);
            if (existing != null && item.Identifier != existing.Identifier)
                return BadRequest("Email già usata");
            manager.Save(item);
            return Ok();
        }

        [HttpGet]
        [Route("User/Delete")]
        public IHttpActionResult Delete(Guid identifier)
        {
            var manager = new UserManager();
            manager.Delete(identifier);
            return Ok();
        }
    }
}