﻿using BLL;
using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Gelaterie.Controllers
{
    [Authorize]
    public class RicettaController : ApiController 
    {
        [HttpGet]
        [Route("Ricetta/GetAll")]
        public IHttpActionResult GetAll()
        {
            var manager = new RicettaManager();

            var ris = manager.GetAll().ToList();

            return Ok(ris);
        }

        [HttpGet]
        [Route("Ricetta/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var manager = new RicettaManager();

            var ris = manager.GetById(identifier);

            return Ok(ris);
        }

        [HttpPost]
        [Route("Ricetta/Save")]
        public IHttpActionResult Save(Ricetta item)
        {
            var manager = new RicettaManager();
            manager.Save(item);
            return Ok();
        }

        [HttpGet]
        [Route("Ricetta/Delete")]
        public IHttpActionResult Delete(Guid identifier)
        {
            var manager = new RicettaManager();
            manager.Delete(identifier);
            return Ok();
        }

        [HttpGet]
        [Route("Ricetta/TogglePreferita")]
        public IHttpActionResult TogglePreferita(Guid identifier)
        {
            var manager = new UserManager();
            manager.ToggleRicettaPreferita(OwinUtility.GetUserIdentifier().Value, identifier);
            return Ok();
        }

        [HttpGet]
        [Route("Ricetta/GetBasi")]
        public IHttpActionResult GetBasi()
        {
            var manager = new RicettaManager();
            var ris = manager.GetAll().Where(x => x.IsBase).ToList();
            return Ok(ris);
        }
    }
}
