﻿using BLL;
using OM;
using OM.Enum;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace Gelaterie.Controllers
{
    public class ConfigController : ApiController
    {
        [HttpGet]
        [Route("Config/Get")]
        public IHttpActionResult Get()
        {
            var mgr = new ConfigManager();
            var config = mgr.GetConfig();
            if (config == null)
                config = new Config();
            return Ok(config);
        }
        [HttpPost]
        [Route("Config/Save")]
        public IHttpActionResult Save(Config config)
        {
            var mgr = new ConfigManager();
            mgr.Save(config);
            return Ok();
        }
        [HttpPost]
        [Route("Config/SaveTranslation")]
        public IHttpActionResult SaveTranslation(SaveTranslationVm toSave)
        {
            var mgr = new ConfigManager();
            var config = mgr.GetConfig();
            config.Translations[toSave.Key] = toSave.Dict;
            mgr.Save(config);
            return Ok();
        }
        [HttpPost]
        [Route("Config/UploadIngredienti")]
        public async Task<IHttpActionResult> UploadIngredienti()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                HttpContext.Current.Request.Files[0] : null;
            if (file != null && file.ContentLength > 0)
            {
                var configManager = new ConfigManager();
                configManager.ReadIngredientiInterni(file.InputStream);
                return Ok();
            }
            return BadRequest("Nessun file trovato");
        }

        [HttpGet]
        [Route("Config/DownloadIngredienti")]
        public async Task<IHttpActionResult> DownloadIngredienti()
        {
            var configManager = new ConfigManager();
            var excelPath = configManager.WriteIngredientiInterni();
            string bookName = "ingredienti.xls";
            var dataBytes = File.ReadAllBytes(excelPath);
            var dataStream = new MemoryStream(dataBytes);

            HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = bookName;
            result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
            ResponseMessageResult responseMessageResult = ResponseMessage(result);
            return responseMessageResult;
        }
    }
}