﻿using BLL;
using OM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Gelaterie.Controllers
{
    [Authorize]
    public class StoricoProduzioneController : ApiController
    {
        [HttpGet]
        [Route("StoricoProduzione/GetById")]
        public IHttpActionResult GetById(Guid identifier)
        {
            var mgr = new StoricoProduzioneManager();
            var ris = mgr.GetById(identifier);
            return Ok(ris);
        }

        [HttpGet]
        [Route("StoricoProduzione/GetAll")]
        public IHttpActionResult GetAll()
        {
            var mgr = new StoricoProduzioneManager();
            var ris = mgr.GetAll();
            return Ok(ris);
        }

        [HttpGet]
        [Route("StoricoProduzione/GetByUserIdentifier")]
        public IHttpActionResult GetByUserIdentifier(Guid userIdentifier)
        {
            var mgr = new StoricoProduzioneManager();
            var ris = mgr.GetByUserIdentifier(userIdentifier).ToList();
            return Ok(ris);
        }

        [HttpPost]
        [Route("StoricoProduzione/Create")]
        public IHttpActionResult Create(StoricoProduzione item)
        {
            var userId = BLL.OwinUtility.GetUserIdentifier();
            if (userId != item.UserIdentifier)
                return Unauthorized();

            var manager = new StoricoProduzioneManager();
            manager.Save(item);
            return Ok();
        }

        [HttpPost]
        [Route("StoricoProduzione/Update")]
        public IHttpActionResult Update(StoricoProduzione item)
        {
            var manager = new StoricoProduzioneManager();
            manager.Save(item);
            return Ok();
        }
    }
}