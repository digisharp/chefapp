import Vue from 'vue';
import { ConfigServices } from '@/services/configServices';
import store from '@/store';
import Languages from '@/components/Languages.vue';
let unwatcher = {};
Vue.directive('language', {
    bind: function (el, binding) {
        // store.watch(store.getters.editLanguage, (val) => {
        //     console.log("From watcher " + val);
        // });
        let translations = {};
        unwatcher[binding.value] = store.subscribe((mutation) => {
            if (mutation.type != 'editLanguage')
                return;
            if (mutation.payload) {
                let component = new Languages({
                    propsData: {
                        transKey: binding.value,
                        translations: translations
                    }
                }).$mount();
                el.appendChild(component.$el);
            }
            else {
                el.querySelector('.languageEdit').remove();
            }
        });
        ConfigServices.config.then(config => {
            translations = config.translations[binding.value];
            if (!translations) {
                translations = {};
                config.languages.forEach(lang => {
                    translations[lang] = "";
                });
            }
            else {
                el.innerHTML = translations[ConfigServices.selectedLanguage];
            }
        });
    },
    unbind: function (el, binding) {
        unwatcher[binding.value]();
    }
});
//# sourceMappingURL=languageDirective.js.map