export function EnumToList(inputEnum) {
    let ris = {};
    for (var prop in inputEnum) {
        if (typeof inputEnum[prop] === 'number') {
            ris[inputEnum[prop]] = prop.replace(/_/g, ' ');
        }
    }
    return ris;
}
export function EnumToStringList(inputEnum) {
    let ris = [];
    for (var prop in inputEnum) {
        if (typeof inputEnum[prop] === 'number') {
            ris.push(prop.replace(/_/g, ' '));
        }
    }
    return ris;
}
export class UserRole {
    static getList() {
        return [UserRole.Cliente, UserRole.Fornitore, UserRole.Operatore, UserRole.Admin];
    }
}
UserRole.Cliente = "Cliente";
UserRole.Fornitore = "Fornitore";
UserRole.Operatore = "Operatore";
UserRole.Admin = "Admin";
export var Gruppo;
(function (Gruppo) {
    Gruppo[Gruppo["Disano"] = 1] = "Disano";
    Gruppo[Gruppo["Digel"] = 2] = "Digel";
    Gruppo[Gruppo["Romeo"] = 3] = "Romeo";
})(Gruppo || (Gruppo = {}));
export var TipoProdotto;
(function (TipoProdotto) {
    TipoProdotto[TipoProdotto["Ingrediente"] = 1] = "Ingrediente";
    TipoProdotto[TipoProdotto["Articolo"] = 2] = "Articolo";
})(TipoProdotto || (TipoProdotto = {}));
export var StatoOrdine;
(function (StatoOrdine) {
    StatoOrdine[StatoOrdine["Inviato"] = 0] = "Inviato";
    StatoOrdine[StatoOrdine["InViaggio"] = 1] = "InViaggio";
})(StatoOrdine || (StatoOrdine = {}));
//# sourceMappingURL=enum.js.map