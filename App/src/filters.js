import Vue from 'vue';
import * as Enum from '@/enum';
import moment from 'moment';
let tipiProdotto = Enum.EnumToList(Enum.TipoProdotto);
Vue.filter('tipoProdotto', function (value) {
    if (value || value == 0) {
        return tipiProdotto[value];
    }
});
let statiOrdine = Enum.EnumToList(Enum.StatoOrdine);
Vue.filter('statoOrdine', function (value) {
    if (value || value == 0) {
        return statiOrdine[value];
    }
});
Vue.filter('formatDate', function (value, showTime) {
    if (value) {
        if (showTime)
            return moment(value).format('YYYY-MM-DD HH:mm:ss');
        else
            return moment(value).format('YYYY-MM-DD');
    }
});
//# sourceMappingURL=filters.js.map