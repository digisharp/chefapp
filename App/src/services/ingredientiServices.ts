import * as OM from '@/model';
import * as VM from '@/viewModel';
import CommonServices from '@/services/commonServices'

class _IngredientiServices extends CommonServices {
    GetAll(){
        return this.defaultGet<VM.IngredienteVm[]>(this.baseUrl + "Ingrediente/GetAll")
    }
    GetById(identifier: string){
        return this.defaultGet<OM.Ingrediente>(this.baseUrl + "Ingrediente/GetById", {
            identifier
        })
    }
    Save(item: OM.Ingrediente){
        return this.defaultPost<void>(this.baseUrl + "Ingrediente/Save", item)
    }
    Delete(identifier: string){
        return this.defaultGet<void>(this.baseUrl + "Ingrediente/Delete", {
            identifier
        })
    }
    constructor(){
        super();
    }
}

export let IngredientiServices = new _IngredientiServices();