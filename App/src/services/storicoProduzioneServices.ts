import * as OM from '@/model';
import CommonServices from '@/services/commonServices'

class _StoricoProduzioneServices extends CommonServices {
    GetAll(){
        return this.defaultGet<OM.StoricoProduzione[]>(this.baseUrl + "StoricoProduzione/GetAll")
    }
    GetByUserIdentifier(userIdentifier: string){
        return this.defaultGet<OM.StoricoProduzione[]>(this.baseUrl + "StoricoProduzione/GetByUserIdentifier", {
            userIdentifier
        })
    }
    GetById(identifier: string){
        return this.defaultGet<OM.StoricoProduzione>(this.baseUrl + "StoricoProduzione/GetById", {
            identifier
        })
    }
    Create(item: OM.StoricoProduzione){
        return this.defaultPost<void>(this.baseUrl + "StoricoProduzione/Create", item)
    }
    Update(item: OM.StoricoProduzione){
        return this.defaultPost<void>(this.baseUrl + "StoricoProduzione/Update", item)
    }
    constructor(){
        super();
    }
}

export let StoricoProduzioneServices = new _StoricoProduzioneServices();