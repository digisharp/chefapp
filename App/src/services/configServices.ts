import * as OM from '@/model';
import * as VM from '@/viewModel';
import CommonServices from '@/services/commonServices';

class _ConfigServices extends CommonServices {
    Get(showSpinner: boolean = false){
        return this.defaultGet<OM.Config>(this.baseUrl + "Config/Get", {}, showSpinner)
    }
    Save(item: OM.Config){
        return this.defaultPost<void>(this.baseUrl + "Config/Save", item)
    }
    UploadFileIngredienti(file: any){
        return this.uploadFileToUrl<void>(this.baseUrl + "Config/UploadIngredienti", file, {});
    }
    DownloadFileIngredienti(showSpinner: boolean = false){
        return this.defaultGet<OM.DownloadFile>(this.baseUrl + "Config/DownloadIngredienti", {}, showSpinner)
    }
    SaveTranslation(vm: VM.SaveTranslationVm){
        return this.defaultPost<void>(this.baseUrl + "Config/SaveTranslation", vm);
    }
    config: Promise<OM.Config>;
    languages: string[];
    selectedLanguage: string;
    Init(){
        let lang = window.localStorage.getItem("selectedLanguage");
        if(lang)
            this.selectedLanguage = lang;
        else
            this.selectedLanguage = 'italiano';

        this.config = new Promise((resolve, reject) => {
            this.Get(false)
            .then( appConfig => {
                resolve(appConfig);
            });
        });
    }
    constructor(){
        super();
    }
}

export let ConfigServices = new _ConfigServices();