import CommonServices from '@/services/commonServices';
class _ProdottoServices extends CommonServices {
    GetAll(perPage = 0, currentPage = 0) {
        return this.defaultGet(this.baseUrl + "Prodotto/GetAll", {
            perPage,
            currentPage
        });
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "Prodotto/GetById", {
            identifier
        });
    }
    GetNomiProdotti(showSpinner = true) {
        return this.defaultGet(this.baseUrl + "Prodotto/GetNomiProdotti", {}, showSpinner);
    }
    GetFornitoriForOrdine(nomeProdotto) {
        return this.defaultGet(this.baseUrl + "Prodotto/GetFornitoriForOrdine", {
            nomeProdotto
        });
    }
    GetProprietaForOrdine(nomeProdotto, fornitoreIdentifier) {
        return this.defaultGet(this.baseUrl + "Prodotto/GetProprietaForOrdine", {
            nomeProdotto,
            fornitoreIdentifier
        });
    }
    GetAllProprietaNames(showSpinner = false) {
        return this.defaultGet(this.baseUrl + "Prodotto/GetAllProprietaNames", {}, showSpinner);
    }
    Update(item) {
        return this.defaultPost(this.baseUrl + "Prodotto/Update", item);
    }
    Create(item) {
        return this.defaultPost(this.baseUrl + "Prodotto/Create", item);
    }
    SelectProdotto(vm) {
        return this.defaultPost(this.baseUrl + "Prodotto/SelectProdotto", vm);
    }
    Delete(identifier) {
        return this.defaultGet(this.baseUrl + "Prodotto/Delete", {
            identifier
        });
    }
    constructor() {
        super();
    }
}
export let ProdottoServices = new _ProdottoServices();
//# sourceMappingURL=prodottoServices.js.map