import * as OM from '@/model';
import * as VM from '@/viewModel';
import CommonServices from '@/services/commonServices'

class _ProdottoServices extends CommonServices {
    GetAll(perPage: number = 0, currentPage: number = 0){
        return this.defaultGet<OM.TableResult<OM.BaseProdotto>>(this.baseUrl + "Prodotto/GetAll", {
            perPage,
            currentPage
        });
    }
    GetById(identifier: string){
        return this.defaultGet<OM.BaseProdotto>(this.baseUrl + "Prodotto/GetById", {
            identifier
        });
    }
    GetNomiProdotti(showSpinner: boolean = true){
        return this.defaultGet<VM.NomeIdentifierTipo[]>(this.baseUrl + "Prodotto/GetNomiProdotti", {}, showSpinner);
    }
    
    GetFornitoriForOrdine(nomeProdotto: string){
        return this.defaultGet<VM.NomeIdentifier[]>(this.baseUrl + "Prodotto/GetFornitoriForOrdine", {
            nomeProdotto
        });
    }
    GetProprietaForOrdine(nomeProdotto: string, fornitoreIdentifier: string){
        return this.defaultGet<OM.KeyValuePair<string, string>[]>(this.baseUrl + "Prodotto/GetProprietaForOrdine", {
            nomeProdotto,
            fornitoreIdentifier
        });
    }
    GetAllProprietaNames(showSpinner: boolean = false){
        return this.defaultGet<string[]>(this.baseUrl + "Prodotto/GetAllProprietaNames", {}, showSpinner);
    }
    Update(item: OM.BaseProdotto){
        return this.defaultPost<void>(this.baseUrl + "Prodotto/Update", item);
    }
    Create(item: VM.NewProdottoVm){
        return this.defaultPost<string>(this.baseUrl + "Prodotto/Create", item);
    }
    SelectProdotto(vm: VM.SelectProdottoVm){
        return this.defaultPost<OM.BaseProdotto>(this.baseUrl + "Prodotto/SelectProdotto", vm);
    }
    Delete(identifier: OM.BaseProdotto){
        return this.defaultGet<void>(this.baseUrl + "Prodotto/Delete", {
            identifier
        });
    }
    constructor(){
        super();
    }
}

export let ProdottoServices = new _ProdottoServices();