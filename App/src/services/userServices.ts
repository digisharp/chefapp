import * as OM from '@/model';
import * as VM from '@/viewmodel';
import CommonServices from '@/services/commonServices'

class _UserServices extends CommonServices {
    GetAll(){
        return this.defaultGet<OM.BaseUser[]>(this.baseUrl + "User/GetAll")
    }
    GetFornitori(){
        return this.defaultGet<OM.Fornitore[]>(this.baseUrl + "User/GetFornitori")
    }
    GetFornitoriForSelect(){
        return this.defaultGet<VM.NomeIdentifier[]>(this.baseUrl + "User/GetFornitoriForSelect")
    }
    GetById(identifier: string){
        return this.defaultGet<OM.BaseUser>(this.baseUrl + "User/GetById", {
            identifier
        })
    }
    SaveCliente(item: OM.Cliente){
        return this.defaultPost<void>(this.baseUrl + "User/SaveCliente", item)
    }
    SaveFornitore(item: OM.Fornitore){
        return this.defaultPost<void>(this.baseUrl + "User/SaveFornitore", item)
    }
    SaveOperatore(item: OM.Operatore){
        return this.defaultPost<void>(this.baseUrl + "User/SaveOperatore", item)
    }
    SaveAdmin(item: OM.Admin){
        return this.defaultPost<void>(this.baseUrl + "User/SaveAdmin", item)
    }
    Update(item: OM.BaseUser){
        return this.defaultPost<void>(this.baseUrl + "User/Update", item)
    }
    Delete(identifier: string){
        return this.defaultGet<void>(this.baseUrl + "User/Delete", {
            identifier
        })
    }
    constructor(){
        super();
    }
}

export let UserServices = new _UserServices();