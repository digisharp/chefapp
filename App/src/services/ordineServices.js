import CommonServices from '@/services/commonServices';
class _OrdineServices extends CommonServices {
    GetAll() {
        return this.defaultGet(this.baseUrl + "Ordine/GetAll");
    }
    GetAllFilteredBy(filterByIndetifier,role) {
        return this.defaultGet(this.baseUrl + "Ordine/GetAllFilteredBy",{filterByIndetifier,role});
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "Ordine/GetById", {
            identifier
        });
    }
    Create(item) {
        return this.defaultPost(this.baseUrl + "Ordine/Create", item);
    }
    Delete(identifier) {
        return this.defaultGet(this.baseUrl + "Ordine/Delete", {
            identifier
        });
    }
    constructor() {
        super();
    }
}
export let OrdineServices = new _OrdineServices();
//# sourceMappingURL=ordineServices.js.map