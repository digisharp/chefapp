import CommonServices from '@/services/commonServices';
class _ConfigServices extends CommonServices {
    Get(showSpinner = false) {
        return this.defaultGet(this.baseUrl + "Config/Get", {}, showSpinner);
    }
    Save(item) {
        return this.defaultPost(this.baseUrl + "Config/Save", item);
    }
    UploadFileIngredienti(file) {
        return this.uploadFileToUrl(this.baseUrl + "Config/UploadIngredienti", file, {});
    }
    DownloadFileIngredienti(showSpinner = false){
        return this.downloadFile(this.baseUrl + "Config/DownloadIngredienti", {}, showSpinner)
    }
    SaveTranslation(vm) {
        return this.defaultPost(this.baseUrl + "Config/SaveTranslation", vm);
    }
    Init() {
        let lang = window.localStorage.getItem("selectedLanguage");
        if (lang)
            this.selectedLanguage = lang;
        else
            this.selectedLanguage = 'italiano';
        this.config = new Promise((resolve, reject) => {
            this.Get(false)
                .then(appConfig => {
                resolve(appConfig);
            });
        });
    }
    constructor() {
        super();
    }
}
export let ConfigServices = new _ConfigServices();
//# sourceMappingURL=configServices.js.map