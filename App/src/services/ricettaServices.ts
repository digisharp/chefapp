import * as OM from '@/model';
import CommonServices from '@/services/commonServices'

class _RicettaServices extends CommonServices {
    GetAll(){
        return this.defaultGet<OM.Ricetta[]>(this.baseUrl + "Ricetta/GetAll")
    }
    GetById(identifier: string){
        return this.defaultGet<OM.Ricetta>(this.baseUrl + "Ricetta/GetById", {
            identifier
        })
    }
    Save(item: OM.Ricetta){
        return this.defaultPost<void>(this.baseUrl + "Ricetta/Save", item)
    }
    Delete(identifier: string){
        return this.defaultGet<void>(this.baseUrl + "Ricetta/Delete", {
            identifier
        })
    }
    TogglePreferita(identifier: string){
        return this.defaultGet<void>(this.baseUrl + "Ricetta/TogglePreferita", {
            identifier
        })
    }
    GetBasi(){
        return this.defaultGet<OM.Ricetta[]>(this.baseUrl + "Ricetta/GetBasi");
    }
    constructor(){
        super();
    }
}

export let RicettaServices = new _RicettaServices();