import CommonServices from './commonServices';
class _AuthServices extends CommonServices {
    login(email, password) {
        var data = "grant_type=password&username=" + email + "&password=" + password;
        return this.defaultPost(this.baseUrl + "token", data, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then((response) => {
            this.setLoginData(JSON.parse(response.jsonLogin));
            console.log(response);
            console.log('Test')
            this.setAuthToken(response.access_token);
            return response;
        });
    }
    register(cliente) {
        return this.defaultPost(this.baseUrl + "User/SaveCliente", cliente)
    }
    logout() {
        let prom = new Promise((resolve, reject) => {
            this.destroyToken();
            resolve();
        });
        return prom;
    }
    constructor() {
        super();
    }
}
export let AuthServices = new _AuthServices();
//# sourceMappingURL=authServices.js.map