import CommonServices from '@/services/commonServices';
class _IngredientiServices extends CommonServices {
    GetAll() {
        return this.defaultGet(this.baseUrl + "Ingrediente/GetAll");
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "Ingrediente/GetById", {
            identifier
        });
    }
    Save(item) {
        return this.defaultPost(this.baseUrl + "Ingrediente/Save", item);
    }
    Delete(identifier) {
        return this.defaultGet(this.baseUrl + "Ingrediente/Delete", {
            identifier
        });
    }
    constructor() {
        super();
    }
}
export let IngredientiServices = new _IngredientiServices();
//# sourceMappingURL=ingredientiServices.js.map