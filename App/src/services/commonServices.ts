import Axios, { AxiosInstance, AxiosPromise, AxiosRequestConfig } from 'axios';
import store from '../store';
import router from '@/router';
import { LoginData } from '@/model';


export default abstract class CommonServices {
    //protected baseUrl = "http://www.furtoincasa.it/api/";
    // protected baseUrl ="http://88.202.230.121/SiChef/";
    protected baseUrl = "http://localhost:55133/";
    //protected baseUrl = "http://192.168.1.5/Gelaterie/";
    private logErrors = true;

    protected showSpinner(){
        store.commit("showSpinner");   
    };
    protected hideSpinner(){
        store.commit("hideSpinner");   
    };
    private resolver(resolve: any, x: any){
        this.hideSpinner();
        resolve(x.data);
    }
    private rejecter(reject: any, err: any){
        this.hideSpinner();
        if(this.logErrors){
            console.error(err);
        }
        reject(err.response.data);
    }
    private axios: AxiosInstance;
    protected defaultGet<T = any>(url: string, params?: any, _showSpinner = true): Promise<T> {
        let req: AxiosRequestConfig = {
            params
        };
        if(_showSpinner)
            this.showSpinner();
        let prom = new Promise<T>((resolve, reject) => {
            this.axios.get<T>(url, req)
            .then(x => {
                this.resolver(resolve, x);
            })
            .catch( err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    protected defaultPost<T = any>(url: string, data?: any, config?: AxiosRequestConfig, _showSpinner = true): Promise<T> {
        if(_showSpinner)
            this.showSpinner()
        let prom = new Promise<T>((resolve, reject) => {
            this.axios.post(url, data, config).then(x => {
                this.resolver(resolve, x);
            })
            .catch( err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    protected uploadFileToUrl<T = any>(url: string, file: File, params: { [key: string]: any }, 
        onUploadProgress?: (progress: number) => void, _showSpinner = true): Promise<T> {
        var data = new FormData();
        if (params) {
            for (var key in params) {
                data.append(key, params[key]);
            }
        }
        data.append('file', file);
        var config = {
            onUploadProgress: function (ev: any) {
                if(typeof onUploadProgress == 'function')
                    onUploadProgress((100 * ev.loaded) / ev.total);
            }
        };
        if(_showSpinner)
            this.showSpinner();
        let prom = new Promise<T>((resolve, reject) => {
            return this.axios.post<T>(url, data, config).then(x => {
                this.resolver(resolve, x);
            })
            .catch( err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    protected setAuthToken(token: string) {
        this.axios.defaults.headers.common['Authorization'] = "Bearer " + token;
        this.axios.defaults.validateStatus = function (status) {
            return status >= 200 && status < 300; // default
        };
        window.localStorage.setItem('authtoken', token);
    }
    protected destroyToken() {
        this.axios.defaults.headers.common['Authorization'] = "";
        window.localStorage.removeItem('authtoken');
    }
    protected setLoginData(data: LoginData){
        window.localStorage.setItem('loginData', JSON.stringify(data));
        store.commit("setLoginData", data);   
    };
    constructor() {
        this.axios = Axios;
        let token = window.localStorage.getItem('authtoken');
        let loginData = window.localStorage.getItem('loginData');
        this.axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {
            if (401 === error.response.status) {
                if(router.currentRoute.path != '/')
                    router.push('/?redirect=' + router.currentRoute.fullPath );
            } else {
                return Promise.reject(error);
            }
        });
        if (token){
            this.setAuthToken(token);
        }
        if(loginData){
            this.setLoginData(JSON.parse(loginData));
        }
    }
}