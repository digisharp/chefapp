import CommonServices from './commonServices';
import * as OM from '@/model';
import { LoginData } from '@/model';

class _AuthServices extends CommonServices {
    public login(email: string, password: string){
        var data = "grant_type=password&username=" + email + "&password=" + password;
        return this.defaultPost<{access_token: string, jsonLogin: string}>(this.baseUrl + "token", data, 
        {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then((response) => {
            this.setLoginData(JSON.parse(response.jsonLogin));
            this.setAuthToken(response.access_token);
            return response;
        });
    }

    public register(item: OM.Cliente) {
        return this.defaultPost<void>(this.baseUrl + "User/SaveCliente", item)
    }

    public logout(){
        let prom = new Promise((resolve, reject) => {
            this.destroyToken();
            resolve();
        })
        return prom;
    }
    constructor(){
        super();
    }
}
export let AuthServices = new _AuthServices();