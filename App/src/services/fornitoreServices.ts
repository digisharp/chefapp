import * as OM from '@/model';
import * as VM from '@/viewModel';
import CommonServices from '@/services/commonServices'

class _FornitoreServices extends CommonServices {
    GetAll(){
        return this.defaultGet<OM.Fornitore[]>(this.baseUrl + "Fornitori/GetAll");
    }
    GetById(identifier: string){
        return this.defaultGet<OM.Fornitore>(this.baseUrl + "Fornitori/GetById", {
            identifier
        });
    }
    Save(fornitore: OM.Fornitore){
        return this.defaultPost<void>(this.baseUrl + "Fornitori/Save", fornitore);
    }
    GetNomeIdentifier(){
        return this.defaultGet<VM.NomeIdentifier[]>(this.baseUrl + "Fornitori/GetNomeIdentifier");
    }
    constructor(){
        super();
    }
}

export let FornitoreServices = new _FornitoreServices();