import Axios from 'axios';
import store from '../store';
import router from '@/router';
export default class CommonServices {
    constructor() {
        //protected baseUrl = "http://www.furtoincasa.it/api/";
        // this.baseUrl = "http://88.202.230.121/SiChef/";
        this.baseUrl  = "http://localhost:55133/";
        //protected baseUrl = "http://192.168.1.5/Gelaterie/";
        // this.baseUrl  = "http://chefapp.it/api/";
        this.logErrors = true;
        this.axios = Axios;
        let token = window.localStorage.getItem('authtoken');
        let loginData = window.localStorage.getItem('loginData');
        this.axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {
            if (401 === error.response.status) {
                if (router.currentRoute.path != '/')
                    router.push('/?redirect=' + router.currentRoute.fullPath);
            }
            else {
                return Promise.reject(error);
            }
        });
        if (token) {
            this.setAuthToken(token);
        }
        if (loginData) {
            this.setLoginData(JSON.parse(loginData));
        }
    }
    showSpinner() {
        store.commit("showSpinner");
    }
    ;
    hideSpinner() {
        store.commit("hideSpinner");
    }
    ;
    resolver(resolve, x) {
        this.hideSpinner();
        resolve(x.data);
    }
    rejecter(reject, err) {
        this.hideSpinner();
        if (this.logErrors) {
            console.error(err);
        }
        reject(err.response.data);
    }
    defaultGet(url, params, _showSpinner = true) {
        let req = {
            params
        };
        if (_showSpinner)
            this.showSpinner();
        let prom = new Promise((resolve, reject) => {
            this.axios.get(url, req)
                .then(x => {
                this.resolver(resolve, x);
            })
                .catch(err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    defaultPost(url, data, config, _showSpinner = true) {
        if (_showSpinner)
            this.showSpinner();
        let prom = new Promise((resolve, reject) => {
            this.axios.post(url, data, config).then(x => {
                this.resolver(resolve, x);
            })
                .catch(err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    uploadFileToUrl(url, file, params, onUploadProgress, _showSpinner = true) {
        var data = new FormData();
        if (params) {
            for (var key in params) {
                data.append(key, params[key]);
            }
        }
        data.append('file', file);
        var config = {
            onUploadProgress: function (ev) {
                if (typeof onUploadProgress == 'function')
                    onUploadProgress((100 * ev.loaded) / ev.total);
            }
        };
        if (_showSpinner)
            this.showSpinner();
        let prom = new Promise((resolve, reject) => {
            return this.axios.post(url, data, config).then(x => {
                this.resolver(resolve, x);
            })
                .catch(err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    downloadFile(url, params, _showSpinner = true) {
        let req = {
            params
        };
        if (_showSpinner)
            this.showSpinner();
        let prom = new Promise((resolve, reject) => {
            this.axios({
                method:'get',
                url:url,
                responseType:'blob'
              })
              .then(x => {
                this.resolver(resolve, x);
            }) .catch(err => {
                this.rejecter(reject, err);
            });
        });
        return prom;
    }
    setAuthToken(token) {
        this.axios.defaults.headers.common['Authorization'] = "Bearer " + token;
        this.axios.defaults.validateStatus = function (status) {
            return status >= 200 && status < 300; // default
        };
        window.localStorage.setItem('authtoken', token);
    }
    destroyToken() {
        this.axios.defaults.headers.common['Authorization'] = "";
        window.localStorage.removeItem('authtoken');
    }
    setLoginData(data) {
        window.localStorage.setItem('loginData', JSON.stringify(data));
        store.commit("setLoginData", data);
    }
    ;
}
//# sourceMappingURL=commonServices.js.map