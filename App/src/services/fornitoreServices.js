import CommonServices from '@/services/commonServices';
class _FornitoreServices extends CommonServices {
    GetAll() {
        return this.defaultGet(this.baseUrl + "Fornitori/GetAll");
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "Fornitori/GetById", {
            identifier
        });
    }
    Save(fornitore) {
        return this.defaultPost(this.baseUrl + "Fornitori/Save", fornitore);
    }
    GetNomeIdentifier() {
        return this.defaultGet(this.baseUrl + "Fornitori/GetNomeIdentifier");
    }
    constructor() {
        super();
    }
}
export let FornitoreServices = new _FornitoreServices();
//# sourceMappingURL=.js.map