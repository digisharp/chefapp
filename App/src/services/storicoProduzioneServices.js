import CommonServices from '@/services/commonServices';
class _StoricoProduzioneServices extends CommonServices {
    GetAll() {
        return this.defaultGet(this.baseUrl + "StoricoProduzione/GetAll");
    }
    GetByUserIdentifier(userIdentifier) {
        return this.defaultGet(this.baseUrl + "StoricoProduzione/GetByUserIdentifier", {
            userIdentifier
        });
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "StoricoProduzione/GetById", {
            identifier
        });
    }
    Create(item) {
        return this.defaultPost(this.baseUrl + "StoricoProduzione/Create", item);
    }
    Update(item) {
        return this.defaultPost(this.baseUrl + "StoricoProduzione/Update", item);
    }
    constructor() {
        super();
    }
}
export let StoricoProduzioneServices = new _StoricoProduzioneServices();
//# sourceMappingURL=storicoProduzioneServices.js.map