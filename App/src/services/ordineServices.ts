import * as OM from '@/model';
import CommonServices from '@/services/commonServices'

class _OrdineServices extends CommonServices {
    GetAll(){
        return this.defaultGet<OM.Ordine[]>(this.baseUrl + "Ordine/GetAll")
    }
    GetAllFilteredBy(filterByIndetifier: string, role: string){
        return this.defaultGet<OM.Ordine[]>(this.baseUrl + "Ordine/GetAll",{filterByIndetifier,role})
    }
    GetById(identifier: string){
        return this.defaultGet<OM.Ordine>(this.baseUrl + "Ordine/GetById", {
            identifier
        })
    }
    Create(item: OM.Ordine){
        return this.defaultPost<void>(this.baseUrl + "Ordine/Create", item)
    }
    Delete(identifier: OM.Ordine){
        return this.defaultGet<void>(this.baseUrl + "Ordine/Delete", {
            identifier
        })
    }
    constructor(){
        super();
    }
}

export let OrdineServices = new _OrdineServices();