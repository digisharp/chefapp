import CommonServices from '@/services/commonServices';
class _RicettaServices extends CommonServices {
    GetAll() {
        return this.defaultGet(this.baseUrl + "Ricetta/GetAll");
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "Ricetta/GetById", {
            identifier
        });
    }
    Save(item) {
        return this.defaultPost(this.baseUrl + "Ricetta/Save", item);
    }
    Delete(identifier) {
        return this.defaultGet(this.baseUrl + "Ricetta/Delete", {
            identifier
        });
    }
    TogglePreferita(identifier) {
        return this.defaultGet(this.baseUrl + "Ricetta/TogglePreferita", {
            identifier
        });
    }
    GetBasi() {
        return this.defaultGet(this.baseUrl + "Ricetta/GetBasi");
    }
    constructor() {
        super();
    }
}
export let RicettaServices = new _RicettaServices();
//# sourceMappingURL=ricettaServices.js.map