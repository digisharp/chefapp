import CommonServices from '@/services/commonServices';
class _UserServices extends CommonServices {
    GetAll() {
        return this.defaultGet(this.baseUrl + "User/GetAll");
    }
    GetFornitori() {
        return this.defaultGet(this.baseUrl + "User/GetFornitori");
    }
    GetFornitoriForSelect() {
        return this.defaultGet(this.baseUrl + "User/GetFornitoriForSelect");
    }
    GetById(identifier) {
        return this.defaultGet(this.baseUrl + "User/GetById", {
            identifier
        });
    }
    SaveCliente(item) {
        return this.defaultPost(this.baseUrl + "User/SaveCliente", item);
    }
    SaveFornitore(item) {
        return this.defaultPost(this.baseUrl + "User/SaveFornitore", item);
    }
    SaveOperatore(item) {
        return this.defaultPost(this.baseUrl + "User/SaveOperatore", item);
    }
    SaveAdmin(item) {
        return this.defaultPost(this.baseUrl + "User/SaveAdmin", item);
    }
    Update(item) {
        return this.defaultPost(this.baseUrl + "User/Update", item);
    }
    Delete(identifier) {
        return this.defaultGet(this.baseUrl + "User/Delete", {
            identifier
        });
    }
    constructor() {
        super();
    }
}
export let UserServices = new _UserServices();
//# sourceMappingURL=userServices.js.map