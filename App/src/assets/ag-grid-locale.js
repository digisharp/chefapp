export const localeText = {
    // for filter panel
    page: 'Pagina',
    more: 'Più',
    to: 'a',
    of: 'di',
    next: 'succ.',
    last: 'ultimo',
    first: 'primo',
    previous: 'prec.',
    loadingOoo: 'caricamento...',
    // // for set filter
    // selectAll: 'daSelect Allen',
    // searchOoo: 'daSearch...',
    // blanks: 'daBlanc',
    // // for number filter and text filter
    // filterOoo: 'daFilter...',
    // applyFilter: 'daApplyFilter...',
    // // for number filter
    // equals: 'daEquals',
    // lessThan: 'daLessThan',
    // greaterThan: 'daGreaterThan',
    // // for text filter
    // contains: 'contiene',
    // startsWith: 'inizia con',
    // endsWith: 'finisce con',
    // // the header of the default group column
    // group: 'laGroup',
    // // tool panel
    // columns: 'laColumns',
    // rowGroupColumns: 'laPivot Cols',
    // rowGroupColumnsEmptyMessage: 'la drag cols to group',
    // valueColumns: 'laValue Cols',
    // pivotMode: 'laPivot-Mode',
    // groups: 'laGroups',
    // values: 'laValues',
    // pivots: 'laPivots',
    // valueColumnsEmptyMessage: 'la drag cols to aggregate',
    // pivotColumnsEmptyMessage: 'la drag here to pivot',
    // toolPanelButton: 'la tool panel',
    // // other
    noRowsToShow: 'La ricerca non ha prodotto risultati',
};
//# sourceMappingURL=ag-grid-locale.js.map