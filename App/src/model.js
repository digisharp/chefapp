import * as Enum from '@/enum';
import store from '@/store';

export class AppRoute {
    constructor(label, routeName, icon) {
        this.label = label,
            this.routeName = routeName,
            this.icon = icon;
    }
}
export class LoginData {
    constructor() {
        this.username = "";
        this.userIdentifier = "";
        this.userRole = Enum.UserRole.Cliente;
    }
}
export class BaseEntity {
    constructor() {
        this.identifier = "";
        this.createdOn = "";
        this.lastSync = null;
        this.creatorRole = store.state.loginData.userRole;
    }
}
export class Address {
    constructor() {
        this.country = "";
        this.latitude = 0;
        this.locality = "";
        this.longitude = 0;
        this.postalCode = "";
        this.province = "";
        this.region = "";
        this.route = "";
        this.streetNumber = "";
        this.typedAddress = "";
    }
}
export class AnagraficaBase {
    constructor() {
        this.nome = "";
        this.referente = "";
        this.partitaIva = "";
        this.telefono = "";
        this.email = "";
    }
}
export class BaseUser extends BaseEntity {
    constructor() {
        super();
        this.username = "";
        this.email = "";
        this.password = "";
        this.role = "";
    }
}
export class Cliente extends BaseUser {
    constructor() {
        super();
        this.indirizzo = new Address();
        this.ricettePreferite = [];
    }
}
export class FornitoreOld extends BaseUser {
    constructor() {
        super();
        this.role = Enum.UserRole.Fornitore;
        this.indirizzo = new Address();
    }
}
export class Fornitore extends BaseEntity {
    constructor() {
        super();
        this.indirizzo = new Address();
    }
}
export class DownloadFile
{
    constructor(){
    }
}

export class ChartsData
{
    constructor(){
        this.labels = [];
    }
    
}
export class Operatore extends BaseUser {
    constructor() {
        super();
        this.role = Enum.UserRole.Operatore;
    }
}
export class Admin extends BaseUser {
    constructor() {
        super();
        this.role = Enum.UserRole.Admin;
    }
}
export class IngredienteSummary extends BaseEntity {
    constructor() {
        super();
        this.ricette = 0;
        this.zone = 0;
        this.intolleranze = 0;
        this.promozioni = 0;
        this.fornitori = 0;
        this.immagini = 0;
    }
}
export class Ricetta extends BaseEntity {
    constructor() {
        super();
        this.ingredienti = [];
        this.basi = [];
    }
}
export class KeyValuePair {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}
export class BaseProdotto extends BaseEntity {
    constructor() {
        super();
        this.proprieta = [];
    }
}
export class Articolo extends BaseProdotto {
    constructor() {
        super();
        this.tipoProdotto = Enum.TipoProdotto.Articolo;
    }
}
export class Ingrediente extends BaseProdotto {
    constructor() {
        super();
        this.tipoProdotto = Enum.TipoProdotto.Ingrediente;
        this.dettaglio = new DettaglioIngrediente();
    }
}
export class DettaglioIngrediente {
    constructor() {
        this.acqua = 0;
        this.lipidi = 0,
            this.proteine = 0,
            this.zuccheri = 0;
        this.fibre = 0;
        this.carboidrati = 0;
        this.ceneri = 0;
        this.pac = 0;
        this.pod = 0;
        this.altriSolidi = 0;
        this.solidiTotali = 0;
    }
}
export class NutrientiIngrediente {
}
export class Config extends BaseEntity {
    constructor() {
        super();
        this.produttori = [];
        this.translations = {};
    }
}
export class Lotto extends BaseEntity {
}
export class Ordine extends BaseEntity {
    constructor() {
        super();
        this.prodotti = [];
    }
}
export class ProdottoOrdine {
    constructor(prod) {
        this.proprieta = [];
        if (prod) {
            this.nomeProdotto = prod.nome;
            this.prodottoIdentifier = prod.identifier;
            this.fornitore = prod.fornitore;
            this.proprieta = prod.proprieta;
            this.fornitoreIdentifier = prod.fornitoreIdentifier;
            this.prezzo = prod.prezzoDiListino;
            this.quantita = prod.tipoProdotto == Enum.TipoProdotto.Articolo ? prod.quantitaUnitaria : prod.quantitaMillesimale;
            this.unitaDiMisura = prod.isQuantitaUnitaria ? "Unità" : "Kg";
        }
    }
}
export class TableResult {
    constructor() {
        this.data = [];
    }
}
export class StoricoProduzione extends BaseEntity {
    constructor() {
        super();
        this.ingredientiUsati = [];
    }
}
export class ConsumoIngrediente {
}
//# sourceMappingURL=model.js.map