export function EnumToList(inputEnum: any): { [key: string]: number | string } {
    let ris: any = {};
    for(var prop in inputEnum){
        if (typeof inputEnum[prop] === 'number') {
            ris[inputEnum[prop]] = prop.replace(/_/g, ' ');
        }
    }
    return ris;
}

export function EnumToStringList(inputEnum: any): string[] {
    let ris: string[] = [];
    for(var prop in inputEnum){
        if (typeof inputEnum[prop] === 'number') {
            ris.push(prop.replace(/_/g, ' '));
        }
    }
    return ris;
}




export class UserRole {
    static Cliente = "Cliente";
    static Fornitore = "Fornitore";
    static Operatore = "Operatore";
    static Admin = "Admin";
    static getList(){
        return [UserRole.Cliente, UserRole.Fornitore, UserRole.Operatore, UserRole.Admin];
    }
}

export enum Gruppo
{
    Disano = 1,
    Digel = 2,
    Romeo = 3
}

export enum TipoProdotto
{
    Ingrediente = 1,
    Articolo = 2
}

export enum StatoOrdine
{
    Inviato,
    InViaggio
}