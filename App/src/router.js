import * as OM from './model' ; 
import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from './views/dashboard/Dashboard.vue';
import Laboratorio from './views/laboratorio/Laboratorio.vue';
import Gelato from './views/laboratorio/Gelato.vue';
import GelatoRicetta from './views/laboratorio/GelatoRicetta.vue';
import StoricoProduzione from './views/laboratorio/StoricoProduzione.vue';
import Login from './views/login/Login.vue';
import Registrare from './views/login/Registrare.vue';
import Layout from './views/layout/Layout.vue';
import Ingredienti from './views/ingredienti/Ingredienti.vue';
import Ordini from './views/ordine/Ordini.vue';
import NuovoOrdine from './views/ordine/NuovoOrdine.vue';
import OrdineDetail from './views/ordine/OrdineDetail.vue';
import NuovoIngrediente from './views/ingredienti/NuovoIngrediente.vue';
import ModificaIngrediente from './views/ingredienti/ModificaIngrediente.vue';
import Ricette from './views/ricette/Ricette.vue';
import NuovaRicetta from './views/ricette/NuovaRicetta.vue';
import RicettaDetail from './views/ricette/RicettaDetail.vue';
import Prodotti from './views/prodotti/Prodotti.vue';
import NuovoProdotto from './views/prodotti/NuovoProdotto.vue';
import ProdottoDetail from './views/prodotti/ProdottoDetail.vue';
import Users from './views/users/Users.vue';
import NuovoUser from './views/users/NuovoUser.vue';
import UserDetail from './views/users/UserDetail.vue';
import Fornitori from './views/fornitori/Fornitori.vue';
import FornitoreDetail from './views/fornitori/DettaglioFornitore.vue';
import Config from './views/config/Config.vue';
Vue.use(Router);
export const adminRoutes = [];
export const clienteRoutes = [];
export const commonRoutes = [];

commonRoutes.push(new OM.AppRoute('Dashboard', '/dashboard', 'mdi mdi-auto-fix'));
commonRoutes.push(new OM.AppRoute('Ordini', '/ordini', 'mdi mdi-cart'));
commonRoutes.push(new OM.AppRoute('Prodotti', '/prodotti', 'mdi mdi-view-list'));
commonRoutes.push(new OM.AppRoute('Ingredienti', '/ingredienti', 'mdi mdi-food-apple'));

clienteRoutes.push(new OM.AppRoute('Laboratorio', '/laboratorio/gelato', 'fa fa-th-large'));
clienteRoutes.push(new OM.AppRoute('Storico', '/laboratorio/storicoProduzione', 'fa fa-bar-chart'));
clienteRoutes.push(new OM.AppRoute('Ricette', '/ricette', 'fa fa-book'));

adminRoutes.push(new OM.AppRoute('Fornitori', '/fornitori', 'fa fa-industry'));
adminRoutes.push(new OM.AppRoute('Utenze', '/users', 'fa fa-users'));
adminRoutes.push(new OM.AppRoute('Config', '/config', 'fa fa-wrench'));

export default new Router({
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login
        }, 
        {
            path: '/registrare',
            name: 'registrare',
            component: Registrare
        },
        {
            path: '/app',
            component: Layout,
            children: [
                {
                    path: '/dashboard',
                    name: 'dashboard',
                    component: Dashboard,
                },
                {
                    path: '/laboratorio',
                    component: Laboratorio,
                },
                {
                    path: '/laboratorio/gelato',
                    name: 'gelato',
                    component: Gelato,
                },
                {
                    path: '/laboratorio/gelato/:ricettaIdentifier',
                    component: GelatoRicetta,
                },
                {
                    path: '/laboratorio/storicoProduzione',
                    component: StoricoProduzione,
                },
                {
                    path: '/ingredienti',
                    component: Ingredienti,
                },
                {
                    path: '/ingredienti/new',
                    component: NuovoIngrediente,
                },
                {
                    path: '/ingredienti/modifica/:identifier',
                    component: ModificaIngrediente,
                },
                {
                    path: '/ordini',
                    component: Ordini,
                },
                {
                    path: '/ordini/new',
                    component: NuovoOrdine, 
                },
                {
                    path: '/ordini/detail/:identifier',
                    component: OrdineDetail,
                },
                {
                    path: '/ricette',
                    component: Ricette,
                },
                {
                    path: '/ricette/new',
                    component: NuovaRicetta,
                },
                {
                    path: '/ricette/detail/:identifier',
                    component: RicettaDetail,
                },
                {
                    path: '/prodotti',
                    component: Prodotti,
                },
                {
                    path: '/prodotti/new',
                    component: NuovoProdotto,
                },
                {
                    path: '/prodotti/detail/:identifier',
                    component: ProdottoDetail,
                },
                {
                    path: '/users',
                    component: Users,
                },
                {
                    path: '/users/new',
                    component: NuovoUser,
                },
                {
                    path: '/users/detail/:identifier',
                    component: UserDetail,
                },
                {
                    path: '/fornitori',
                    component: Fornitori,
                },
                {
                    path: '/fornitori/detail/:identifier',
                    component: FornitoreDetail,
                },
                {
                    path: '/config',
                    component: Config,
                }
            ]
        },
    ],
});
//# sourceMappingURL=router.js.map