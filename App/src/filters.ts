import Vue from 'vue';
import * as Enum from '@/enum';
import moment from 'moment';

let tipiProdotto = Enum.EnumToList(Enum.TipoProdotto)
Vue.filter('tipoProdotto', function(value: any) {
    if (value || value == 0) {
        return tipiProdotto[value];
    }
})

let statiOrdine = Enum.EnumToList(Enum.StatoOrdine);
Vue.filter('statoOrdine', function(value: any) {
    if (value || value == 0) {
        return statiOrdine[value];
    }
})

Vue.filter('formatDate', function(value: any, showTime: boolean) {
    if (value) {
        if(showTime)
            return moment(value).format('YYYY-MM-DD HH:mm:ss');
        else
            return moment(value).format('YYYY-MM-DD');
    }
})