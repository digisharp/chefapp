import '@/css/bootstrap.min.css';
import '@/css/side-menu.css';
import '@/css/animate.css';
import '@/css/font-awesome.min.css';
import '@/css/toastr.min.css';
import '@/css/ag-grid.css';
import '@/css/ag-theme-bootstrap.css';
import './vendors/iconfonts/mdi/css/materialdesignicons.min.css';
import './vendors/css/vendor.bundle.base.css';
//import './vendors/css/vendor.bundle.addons.css';
import './css/style.css';
import './css/app-style.css';
import './js/jquery-3.1.0.min.js';
import './js/bootstrap.min.js';
import './filters';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './directives/languageDirective';
//Vue.config.productionTip = false;
import toastr from 'toastr';
import VeeValidate, { Validator } from 'vee-validate';
import itLocale from '@/assets/vee-validate-it';
Validator.localize('it', itLocale);
Vue.use(VeeValidate);
import { ConfigServices } from '@/services/configServices';
ConfigServices.Init();
toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: true,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    showDuration: 300,
    hideDuration: 1000,
    timeOut: 4000,
    extendedTimeOut: 1000,
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
};
new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount('#app');
//# sourceMappingURL=main.js.map