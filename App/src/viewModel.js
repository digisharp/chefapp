export class IngredienteVm {
}
export class SaveTranslationVm {
}
export class NewProdottoVm {
    constructor() {
        this.nome = "";
        this.tipoProdotto = null;
        this.fornitoreIdentifier = "";
        this.fornitore = "";
        this.proprieta = [];
        this.prezzoDiListino = null;
        this.quantitaUnitaria = null;
        this.unitaDiMisura = "";
        this.isQuantitaUnitaria = false;
        this.quantitaMillesimale = null;
    }
}
export class NomeIdentifier {
}
export class NomeIdentifierTipo {
}
export class SelectProdottoVm {
    constructor() {
        this.proprieta = [];
    }
}
export class QuantitaIngredienteVm {
}
export class RicettaBase {
}
//# sourceMappingURL=viewModel.js.map