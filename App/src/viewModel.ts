import * as Enum from '@/enum';
import * as OM from '@/model';

export class IngredienteVm
{
    identifier: string;
    nome: string;
    fornitore: string;
    categoria: string;
    prezzoDiListino: number;
    quantitaMillesimale:number;
    nomeIngredienteFornitore: string;
}

export class SaveTranslationVm
{
    key: string;
    dict: { [key: string ]: string };
}

export class NewProdottoVm 
{
    nome: string;
    tipoProdotto: Enum.TipoProdotto;
    fornitoreIdentifier: string;
    fornitore: string;
    proprieta: OM.KeyValuePair<string, string>[];
    prezzoDiListino: number;
    quantitaUnitaria: number;
    unitaDiMisura: string;
    isQuantitaUnitaria: boolean;
    quantitaMillesimale: number;
    immagine: string;
    constructor(){
        this.nome = "";
        this.tipoProdotto = null;
        this.fornitoreIdentifier = "";
        this.fornitore = "";
        this.proprieta = [];
        this.prezzoDiListino = null;
        this.quantitaUnitaria = null;
        this.unitaDiMisura = "";
        this.isQuantitaUnitaria = false;
        this.quantitaMillesimale = null;
        this.immagine = "";
    }
}

export class NomeIdentifier {
    nome: string;
    identifier: string;
}

export class NomeIdentifierTipo
{
    nome: string;
    identifier: string;
    tipo: Enum.TipoProdotto;
}

export class SelectProdottoVm {
    nome: string;
    fornitoreIdentifier: string;
    proprieta: OM.KeyValuePair<string, string>[];
    constructor(){
        this.proprieta = [];
    }
}

export class QuantitaIngredienteVm
{
    identifier: string;
    quantita: number;
    quantitaPercentuale: number;
    nome: string;
    dettaglio: OM.DettaglioIngrediente;
    costoAlGrammo: number;
    fornitore: string;
}

export class RicettaBase {
    identifier: string;
    nome: string;
    quantita: number;
    ingredienti: QuantitaIngredienteVm[];
}