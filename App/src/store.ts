import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import * as OM from '@/model';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        showSpinner: 0,
        loginData: new OM.LoginData(),
        editLanguage: false
    },
    mutations: {
        showSpinner(state){
            state.showSpinner++;
        },
        hideSpinner(state){
            state.showSpinner--;
            if(state.showSpinner < 0)
                state.showSpinner = 0;
        },
        setLoginData(state, data: OM.LoginData){
            state.loginData = data;
        },
        editLanguage(state, data: boolean){
            state.editLanguage = data;
        }
    },
    getters: {
        editLanguage: (state) => {
            return () => state.editLanguage;
        }
    }
});
