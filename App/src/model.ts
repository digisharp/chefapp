import * as Enum from '@/enum';
import * as VM from '@/viewModel';
import store from '@/store';

export class AppRoute 
{
    label: string;
    routeName: string;
    icon: string;
    constructor(label: string, routeName: string, icon: string){
        this.label = label,
        this.routeName = routeName,
        this.icon = icon
    }
}

export class LoginData 
{
    username: string;
    userIdentifier: string;
    userRole: Enum.UserRole;
    constructor(){
        this.username = "";
        this.userIdentifier = "";
        this.userRole = Enum.UserRole.Cliente;
    }
}

export class BaseEntity 
{
    identifier: string;
    createdOn: string;
    createdBy: string;
    creatorRole: Enum.UserRole;
    lastSync?: string | null; 
    constructor(){
        this.identifier = "";
        this.createdOn = "";
        this.lastSync = null;
    }
}

export class Address 
{
    typedAddress: string;
    streetNumber: string;
    route: string;
    locality: string;
    province: string;
    region: string;
    country: string;
    postalCode: string;
    latitude: number;
    longitude: number;
    constructor()
    {
        this.country = "";
        this.latitude = 0;
        this.locality = "";
        this.longitude = 0;
        this.postalCode = "";
        this.province = "";
        this.region = "";
        this.route = "";
        this.streetNumber = "";
        this.typedAddress = "";
    }
}

export class AnagraficaBase 
{
    nome: string;
    referente: string;
    partitaIva: string;
    telefono: string;
    email: string;
    constructor(){
        this.nome = "";
        this.referente = "";
        this.partitaIva = "";
        this.telefono = "";
        this.email = "";
    }
}

export class BaseUser extends BaseEntity
{
    username: string;
    email: string;
    password: string;
    numTelefono: string;
    role: Enum.UserRole;
    constructor(){
        super();
        this.username = "";
        this.email = "";
        this.password = "";
        this.role = "";
    }
}

export class Cliente extends BaseUser
{
    ragioneSociale: string;
    partitaIva: string;
    indirizzo: Address;
    referente: string;
    ricettePreferite: string[];
    constructor(){
        super();
        this.indirizzo = new Address();
        this.ricettePreferite = [];
    }
}

export class FornitoreOld extends BaseUser
{
    ragioneSociale: string;
    partitaIva: string;
    referente: string;
    indirizzo: Address;
    constructor(){
        super();
        this.role = Enum.UserRole.Fornitore;
        this.indirizzo = new Address();
    }
}

export class Fornitore extends BaseEntity
{
    ragioneSociale: string;
    partitaIva: string;
    email: string;
    indirizzo: Address;
    referente: string;
    constructor(){
        super();
        this.indirizzo = new Address();
    }
}

export class DownloadFile
{
    constructor(){
    }
}

export class Operatore extends BaseUser
{
    nome: string;
    cognome: string;
    constructor(){
        super();
        this.role = Enum.UserRole.Operatore;
    }
}
export class Admin extends BaseUser
{
    constructor(){
        super();
        this.role = Enum.UserRole.Admin;
    }
}

export class IngredienteSummary extends BaseEntity
{
    ricette: number;
    zone: number;
    intolleranze: number;
    promozioni: number;
    fornitori: number;
    immagini: number;
    constructor(){
        super();
        this.ricette = 0;
        this.zone = 0;
        this.intolleranze = 0;
        this.promozioni = 0;
        this.fornitori = 0;
        this.immagini = 0;
    }
}

export class Ricetta extends BaseEntity
{
    nome: string;
    costo: number;
    isBase: boolean;
    classificazione:string;
    ingredienti: VM.QuantitaIngredienteVm[];
    preferita?: boolean;
    overrun: number;
    ordineDiProduzione:number;
    note: string;
    descrizionePreparazione: string;
    basi: VM.RicettaBase[];
    constructor(){
        super();
        this.ingredienti = [];
        this.basi = [];
        this.creatorRole = store.state.loginData.userRole;
    }
}

export class KeyValuePair<T1, T2>
{
    key: T1;
    value: T2;
    constructor(key: T1, value: T2){
        this.key = key;
        this.value = value;
    }
}
export class BaseProdotto extends BaseEntity
{
    nome: string;
    tipoProdotto: Enum.TipoProdotto;
    fornitoreIdentifier: string;
    fornitore: string;
    proprieta: KeyValuePair<string, string>[];
    prezzoDiListino: number;
    quantitaMillesimale?: number;
    quantitaUnitaria?: number;
    unitaDiMisura: string;
    isQuantitaUnitaria: boolean;
    dettaglio?: object;
    constructor(){
        super();
        this.proprieta = [];
    }
}

export class Articolo extends BaseProdotto
{
    tipoProdotto: Enum.TipoProdotto;
    constructor(){
        super();
        this.tipoProdotto = Enum.TipoProdotto.Articolo;
    }
}

export class Ingrediente extends BaseProdotto {
    tipoProdotto: Enum.TipoProdotto;
    dettaglio: DettaglioIngrediente;
    costoAlGrammo: number;
    constructor() 
    {
        super();
        this.tipoProdotto = Enum.TipoProdotto.Ingrediente;
        this.dettaglio = new DettaglioIngrediente();
    }
}

export class DettaglioIngrediente
{
    acqua: number;
    lipidi: number;
    proteine: number;
    zuccheri: number;
    fibre: number;
    carboidrati: number;
    ceneri: number;
    pac: number;
    pod: number;
    altriSolidi: number;
    solidiTotali: number;
    constructor(){
        this.acqua = 0;
        this.lipidi = 0,
        this.proteine = 0,
        this.zuccheri = 0;
        this.fibre = 0;
        this.carboidrati = 0;
        this.ceneri = 0;
        this.pac = 0;
        this.pod = 0;
        this.altriSolidi = 0;
        this.solidiTotali = 0;
    }
}

export class NutrientiIngrediente
{
    gruppoIdentifier: string;
    gruppoNome: string;
    alimento: string;
    kiloCalorie: number;
    glucidi: number;
    proteine: number;
    carboidrati: number;
    ceneri: number;
    grassi: number;
    diCuiSaturi: number;
    fibra: number;
    colesterolo: number;
    kiloJoules: number;
    sodio: number;
    calcio: number;
    ferro: number;
    vitaminaA: number;
    vitaminaC: number;
}

export class Config extends BaseEntity
{
    produttori: string[];
    classificazioniRicette: string[];
    languages: string[];
    translations: {[key: string]: {[key: string]: string }};
    constructor(){
        super();
        this.produttori = [];
        this.translations = {};
    }
}

export class Lotto extends BaseEntity
{
    codiceLotto: string;
    fornitoreIdentifier: string;
    fornitoreNome: string;
    dataColtivazione: string;
    dataImpacchettamento: string;
    dataScadenza: string;
    descrizione: string;
}

export class Ordine extends BaseEntity
{
    clienteIdentifier: string;
    codiceOrdine: string;
    nomeCliente: string;
    prodotti: ProdottoOrdine[];
    statoOrdine: Enum.StatoOrdine;
    totale: number;
    constructor(){
        super();
        this.prodotti = [];
    }
}
export class ProdottoOrdine
{
    nomeProdotto: string;
    prodottoIdentifier: string;
    fornitore: string;
    fornitoreIdentifier: string;
    prezzo: number;
    tipoProdotto: Enum.TipoProdotto;
    proprieta: KeyValuePair<string, string>[];
    quantita: number;
    unitaDiMisura: string;
    codiceLotto: string;
    dataSpedizione: string;
    ddt: string;
    constructor(prod?: BaseProdotto)
    {
        this.proprieta = [];
        if(prod){
            this.nomeProdotto = prod.nome;
            this.prodottoIdentifier = prod.identifier;
            this.fornitore = prod.fornitore;
            this.proprieta = prod.proprieta;
            this.fornitoreIdentifier = prod.fornitoreIdentifier;
            this.prezzo = prod.prezzoDiListino;
            this.quantita = prod.tipoProdotto == Enum.TipoProdotto.Articolo ? prod.quantitaUnitaria : prod.quantitaMillesimale;
            this.unitaDiMisura = prod.isQuantitaUnitaria ? "Unità" : "Kg";
        }
    }
}

export class TableResult<T = any> {
    total: number;
    perPage: number;
    currentPage: number;
    lastPage: number;
    data: T[];
    constructor(){
        this.data = [];
    }
}

export class StoricoProduzione extends BaseEntity
{
    userIdentifier: string;
    ricettaIdentifier: string;
    nomeRicetta: string;
    quantitaGelato: number;
    ingredientiUsati: ConsumoIngrediente[];
    constructor()
    {
        super();
        this.ingredientiUsati = [];
    }
}

export class ConsumoIngrediente
{
    ingredienteIdentifier: string;
    nomeIngrediente: string;
    quantita: number;
}