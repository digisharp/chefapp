module.exports = {
    publicPath: './',
    outputDir: './www',
    configureWebpack: {
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js'
            }
        },
        // module: {
        //     loaders: [
        //         {
        //             test   : /\.css$/,
        //             loaders: ['style-loader', 'css-loader', 'resolve-url-loader']
        //         }, {
        //             test   : /\.scss$/,
        //             loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader?sourceMap']
        //         }
        //     ]
        // }
    }
}