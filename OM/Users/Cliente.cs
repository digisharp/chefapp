﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Users
{
    public class Cliente : BaseUser
    {
        public string RagioneSociale { get; set; }
        public string PartitaIva { get; set; }
        public Address Indirizzo { get; set; }
        public string Referente { get; set; }
        public List<Guid> RicettePreferite { get; set; }
        public override string Role { get; set; }
        public Cliente()
        {
            this.Indirizzo = new Address();
            this.RicettePreferite = new List<Guid>();
            this.Role = UserRole.Cliente;
        }
    }
}
