﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Users
{
    public class BaseUser : BaseEntity
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //Aggiunte Antonio
        public string NumTelefono { get; set; }
        
        public virtual string Role { get; set; }
        public BaseUser()
        {
        }
    }
}
