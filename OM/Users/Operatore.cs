﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Users
{
    public class Operatore : BaseUser
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public override string Role { get; set; }
        public Operatore()
        {
            this.Role = UserRole.Operatore;
        }
    }
}
