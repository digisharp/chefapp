﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Users
{
    public class Admin : BaseUser
    {
        public override string Role { get; set; }
        public Admin()
        {
            this.Role = UserRole.Admin;
        }
    }
}
