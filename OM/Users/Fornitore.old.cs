﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Users
{
    public class FornitoreOld : BaseUser
    {
        public string RagioneSociale { get; set; }
        public string PartitaIva { get; set; }
        public Address Indirizzo { get; set; }
        public override string Role { get; set; }
        public FornitoreOld()
        {
            this.Role = UserRole.Fornitore;
            this.Indirizzo = new Address();
        }
    }
}
