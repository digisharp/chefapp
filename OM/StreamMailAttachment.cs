﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class StreamMailAttachment
    {
        public Stream Stream { get; set; }
        public string NomeFile { get; set; }
        public string MimeType { get; set; }
    }
}
