﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class Lotto : BaseEntity
    {
        public string CodiceLotto { get; set; }
        public Guid FornitoreIdentifier { get; set; }
        public string FornitoreNome { get; set; }
        public DateTime DataColtivazione { get; set; }
        public DateTime DataImpacchettamento { get; set; }
        public DateTime DataScadenza { get; set; }
        public string Descrizione { get; set; }
    }
}
