﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class Fornitore : BaseEntity
    {
        public string RagioneSociale { get; set; }
        public string PartitaIva { get; set; }
        public string Email { get; set; }
        public Address Indirizzo { get; set; }
        public Fornitore()
        {
            this.Indirizzo = new Address();
        }
    }
}
