﻿using OM.Prodotti;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class Ricetta : BaseEntity
    {
        public string Nome { get; set; }
        public decimal Costo { get; set; }
        public int TempoProduzioneMinuti { get; set; }
        public bool IsBase { get; set; }
        public string Classificazione { get; set; }
        public int overrun { get; set; }
        public int OrdineDiProduzione { get; set; }
        public string Note { get; set; }
        public string DescrizionePreparazione { get; set; }
        public List<QuantitaIngredienteVm> Ingredienti { get; set; }
        public List<RicettaBase> Basi { get; set; }
    }
}
