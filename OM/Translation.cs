﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class Translation
    {
        public Dictionary<string, string> Languages { get; set; }
        public Translation()
        {
            Languages = new Dictionary<string, string>();
        }
    }
}
