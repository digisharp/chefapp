﻿using OM.Enum;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Prodotti
{
    public class Articolo : BaseProdotto
    {
        public override TipoProdotto TipoProdotto { get; set; }
        //public override object Dettaglio { get; set; }
        public Articolo() : base()
        {
            TipoProdotto = TipoProdotto.Articolo;
        }
        public Articolo(NewProdottoVm vm) : base(vm)
        {
            TipoProdotto = TipoProdotto.Articolo;
        }
    }
}
