﻿using MongoDB.Bson.Serialization.Attributes;
using OM.Enum;
using OM.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Prodotti
{
    public class BaseProdotto : BaseEntity
    {
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public string ProdottoDa { get; set; }
        public string DimensioniProdotto { get; set; }
        public virtual TipoProdotto TipoProdotto { get; set; }
        public Guid FornitoreIdentifier { get; set; }
        public string Fornitore { get; set; }
        public List<KeyValuePair<string, string>> Proprieta { get; set; }
        public decimal PrezzoDiListino { get; set; }
        public decimal? QuantitaMillesimale { get; set; }
        public int? QuantitaUnitaria { get; set; }
        public string UnitaDiMisura { get; set; }
        public bool IsQuantitaUnitaria { get; set; }
        public virtual object Dettaglio { get; set; }
        public string Immagine { get; set; }
   
        public BaseProdotto() : base()
        {
            this.Proprieta = new List<KeyValuePair<string, string>>();
        }
        public BaseProdotto(NewProdottoVm vm) : base()
        {
            this.Fornitore = vm.Fornitore;
            this.FornitoreIdentifier = vm.FornitoreIdentifier;
            this.IsQuantitaUnitaria = vm.IsQuantitaUnitaria;
            this.Nome = vm.Nome;
            this.PrezzoDiListino = vm.PrezzoDiListino;
            this.Proprieta = vm.Proprieta;
            this.QuantitaMillesimale = vm.QuantitaMillesimale;
            this.QuantitaUnitaria = vm.QuantitaUnitaria;
            this.TipoProdotto = vm.TipoProdotto;
            this.UnitaDiMisura = vm.UnitaDiMisura;
            this.Immagine = vm.Immagine;
        }
    }
}
