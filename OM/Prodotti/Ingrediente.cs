﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.Enum;
using MongoDB.Bson.Serialization.Attributes;
using OM.Viewmodels;

namespace OM.Prodotti
{
    public class Ingrediente : BaseProdotto
    {
        public override TipoProdotto TipoProdotto { get; set; }
        public new DettaglioIngrediente Dettaglio { get; set; }
        public Ingrediente() : base()
        {
            TipoProdotto = TipoProdotto.Ingrediente;
            Dettaglio = new DettaglioIngrediente();
        }
        public Ingrediente(NewProdottoVm vm) : base(vm)
        {
            TipoProdotto = TipoProdotto.Ingrediente;
            Dettaglio = new DettaglioIngrediente();
        }
        public decimal CostoAlGrammo
        {
            get
            {
                if (this.QuantitaMillesimale.HasValue)
                    return this.PrezzoDiListino / (this.QuantitaMillesimale.Value * 1000);
                else
                    return 0;
            }
        }
    }
    public class DettaglioIngrediente
    {
        public decimal Acqua { get; set; }
        public decimal Lipidi { get; set; }
        public decimal Proteine { get; set; }
        public decimal Zuccheri { get; set; }
        public decimal Fibre { get; set; }
        public decimal Carboidrati { get; set; }
        public decimal Ceneri { get; set; }
        public decimal AltriSolidi { get; set; }
        public decimal Pac { get; set; }
        public decimal Pod { get; set; }
        public decimal Kcal { get; set; }
        public decimal Kjoul { get; set; }
        public decimal SolidiTotali { get
            {
                return Acqua + Lipidi + Proteine + Zuccheri + Fibre + Carboidrati + Ceneri + AltriSolidi;
            }
        }
    }
    //public class DettaglioIngrediente
    //{
    //    public string ClasseIngrediente { get; set; }
    //    public string Composizione { get; set; }
    //    public string CodiceProdotto { get; set; }
    //    public decimal Calorie { get; set; }
    //    public decimal Zuccheri { get; set; }
    //    public bool IsSaccarosio { get; set; }
    //    public decimal PercentualeSuSaccarosio { get; set; }
    //    public bool IsZucchero { get; set; }
    //    public decimal MassaMolecolare { get; set; }
    //    public decimal Acqua { get; set; }
    //    public decimal Grassi { get; set; }
    //    public decimal Slng { get; set; }
    //    public decimal Lattosio { get; set; }
    //    public decimal Pac { get; set; }
    //    public decimal Pod { get; set; }
    //    public decimal Alcool { get; set; }
    //    public decimal AltriGlucidi { get; set; }
    //    public decimal AltreProteine { get; set; }
    //    public decimal AltriSolidi { get; set; }
    //    public decimal SolidiTotali { get; set; }
    //    public decimal Umidita { get; set; }
    //    public bool IsBase { get; set; }
    //    public bool IsNoFood { get; set; }
    //    public bool IsSemilavoratoInProprio { get; set; }
    //    public string UnitaDiAcquisto { get; set; }
    //    public decimal ContenutoInGrammi { get; set; }
    //    public decimal PrezzoUnitaDiAcquisto { get; set; }
    //    public decimal CostoAlGrammo { get; set; }
    //    public string UnitaDose { get; set; }
    //    public NutrientiIngrediente Nutrienti { get; set; }
    //    public DettaglioIngrediente()
    //    {
    //        this.Nutrienti = new NutrientiIngrediente();
    //    }
    //}
    //public class NutrientiIngrediente
    //{
    //    public Guid GruppoIdentifier { get; set; }
    //    public string GruppoNome { get; set; }
    //    public string Alimento { get; set; }
    //    public decimal KiloCalorie { get; set; }
    //    public decimal Glucidi { get; set; }
    //    public decimal Proteine { get; set; }
    //    public decimal Carboidrati { get; set; }
    //    public decimal Ceneri { get; set; }
    //    public decimal Grassi { get; set; }
    //    public decimal DiCuiSaturi { get; set; }
    //    public decimal Fibra { get; set; }
    //    public decimal Colesterolo { get; set; }
    //    public decimal KiloJoules { get; set; }
    //    public decimal Sodio { get; set; }
    //    public decimal Calcio { get; set; }
    //    public decimal Ferro { get; set; }
    //    public decimal VitaminaA { get; set; }
    //    public decimal VitaminaC { get; set; }
    //}
}
