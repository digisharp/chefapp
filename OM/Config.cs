﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class Config : BaseEntity
    {
        public List<string> ClassificazioniRicette { get; set; }
        public List<string> Languages { get; set; }
        public Dictionary<string, Dictionary<string, string>> Translations { get; set; }
        public Config()
        {
            this.Translations = new Dictionary<string, Dictionary<string, string>> ();
        }
    }
}
