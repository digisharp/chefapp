﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class StoricoProduzione : BaseEntity
    {
        public Guid UserIdentifier { get; set; }
        public Guid RicettaIdentifier { get; set; }
        public string NomeRicetta { get; set; }
        public decimal QuantitaGelato { get; set; }
        public List<ConsumoIngrediente> IngredientiUsati { get; set; }
        public StoricoProduzione()
        {
            IngredientiUsati = new List<ConsumoIngrediente>();
        }
    }
}
