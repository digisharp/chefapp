﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class ConsumoIngrediente
    {
        public Guid IngredienteIdentifier { get; set; }
        public string NomeIngrediente { get; set; }
        public decimal Quantita { get; set; }
    }
}
