﻿using OM.Enum;
using OM.Prodotti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class ProdottoOrdine
    {
        public string NomeProdotto { get; set; }
        public Guid ProdottoIdentifier { get; set; }
        public string Fornitore { get; set; }
        public Guid FornitoreIdentifier { get; set; }
        public decimal Prezzo { get; set; }
        public TipoProdotto TipoProdotto { get; set; }
        public decimal Quantita { get; set; }
        public string UnitaDiMisura { get; set; }
        public string CodiceLotto { get; set; }
        public List<KeyValuePair<string, string>> Proprieta { get; set; }
        public DateTime DataSpedizione { get; set; }
        public string Ddt { get; set; }
        public ProdottoOrdine() { }
        public ProdottoOrdine(BaseProdotto prod)
        {
            NomeProdotto = prod.Nome;
            ProdottoIdentifier = prod.Identifier;
            Fornitore = prod.Fornitore;
            FornitoreIdentifier = prod.FornitoreIdentifier;
            Prezzo = prod.PrezzoDiListino;
            Quantita = prod.TipoProdotto == Enum.TipoProdotto.Articolo ? (decimal)prod.QuantitaUnitaria.Value : prod.QuantitaMillesimale.Value;
            UnitaDiMisura = prod.IsQuantitaUnitaria ? "Unità" : "Kg";
            Proprieta = new List<KeyValuePair<string, string>>();
        }
    }
}
