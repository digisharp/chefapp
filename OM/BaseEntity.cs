﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class BaseEntity : IBaseEntity
    {
        public Guid Identifier { get; set; }
        public bool Enabled { get; set; }
        public Guid? CreatedBy { get; set; }
        public String CreatorRole { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? LastSync { get; set; }
        public BaseEntity()
        {
            this.Identifier = new Guid();
            this.CreatedOn = DateTime.Now;
            this.CreatedOn = DateTime.Now;
            this.ModifiedBy = null;
            this.CreatedBy = null;
            this.LastSync = null;
            this.Enabled = true;
        }
    }

    public interface IBaseEntity
    {
        Guid Identifier { get; set; }
        Guid? CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        DateTime ModifiedOn { get; set; }
        Guid? ModifiedBy { get; set; }
        DateTime? LastSync { get; set; }
    }
}
