﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class IngredienteSummary : BaseEntity
    {
        public int Ricette { get; set; }
        public int Zone { get; set; }
        public int Intolleranze { get; set; }
        public int Promozioni { get; set; }
        public int Fornitori { get; set; }
        public int Immagini { get; set; }
    }
}
