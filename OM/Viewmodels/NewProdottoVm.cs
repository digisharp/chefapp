﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class NewProdottoVm
    {
        public string Nome { get; set; }
        public TipoProdotto TipoProdotto { get; set; }
        public Guid FornitoreIdentifier { get; set; }
        public string Fornitore { get; set; }
        public List<KeyValuePair<string, string>> Proprieta { get; set; }
        public decimal PrezzoDiListino { get; set; }
        public int QuantitaUnitaria { get; set; }
        public string UnitaDiMisura { get; set; }
        public bool IsQuantitaUnitaria { get; set; }
        public decimal QuantitaMillesimale { get; set; }
        public string Immagine { get; set; }
    }
}
