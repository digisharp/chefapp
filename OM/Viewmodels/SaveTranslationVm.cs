﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class SaveTranslationVm
    {
        public string Key { get; set; }
        public Dictionary<string, string> Dict { get; set; }
    }
}
