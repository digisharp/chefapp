﻿using OM.Prodotti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class QuantitaIngredienteVm
    {
        public Guid Identifier { get; set; }
        public int Quantita { get; set; }
        public string Nome { get; set; }
        public decimal CostoAlGrammo { get; set; }
        public DettaglioIngrediente Dettaglio { get; set; }
    }
}
