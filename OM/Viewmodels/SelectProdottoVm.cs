﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class SelectProdottoVm
    {
        public string Nome { get; set; }
        public Guid FornitoreIdentifier { get; set; }
        public List<KeyValuePair<string, string>> Proprieta { get; set; }
    }
}
