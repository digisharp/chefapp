﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class IngredienteVm
    {
        public Guid Identifier { get; set; }
        public string Nome { get; set; }
        public string Fornitore { get; set; }
        public decimal PrezzoDiListino { get; set; }
        public decimal? QuantitaMillesimale { get; set; }
        public string NomeIngredienteFornitore { get; set; }
    }
}
