﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class NomeIdentifier
    {
        public string Nome { get; set; }
        public Guid Identifier { get; set; }
    }
}
