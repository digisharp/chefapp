﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Viewmodels
{
    public class RicettaBase
    {
        public Guid Identifier { get; set; }
        public string Nome { get; set; }
        public decimal Quantita { get; set; }
        public List<QuantitaIngredienteVm> Ingredienti { get; set; }
    }
}
