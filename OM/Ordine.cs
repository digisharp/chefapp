﻿using OM.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class Ordine : BaseEntity
    {
        public string CodiceOrdine { get; set; }
        public Guid ClienteIdentifier { get; set; }
        public string NomeCliente { get; set; }
        public List<ProdottoOrdine> Prodotti { get; set; }
        public StatoOrdine StatoOrdine { get; set; }
        public decimal Totale { get
            {
                return Prodotti.Sum(x => x.Prezzo);
            }
        }
        public Ordine()
        {
            Prodotti = new List<ProdottoOrdine>();
        }
    }
}
