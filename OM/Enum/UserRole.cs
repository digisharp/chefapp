﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.Enum
{
    public class UserRole
    {
        public const string Operatore = "Operatore";
        public const string Cliente = "Cliente";
        public const string Fornitore = "Fornitore";
        public const string Admin = "Admin";
    }
}
