﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{
    public class TableResult<T>
    {
        public int Total { get; set; }
        public int Per_page { get; set; }
        public int Current_page { get; set; }
        public int Last_page { get; set; }
        public List<T> Data { get; set; }
    }
}
