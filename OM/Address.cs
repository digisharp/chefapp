﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM
{

    public class Address
    {
        public string TypedAddress { get; set; }
        public string StreetNumber { get; set; }
        public string Route { get; set; }
        public string Locality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public string CompleteAddress()
        {
            if (string.IsNullOrEmpty(Locality))
                return string.Empty;
            var address = string.Empty;
            if (!string.IsNullOrEmpty(Route))
                address += Route + ", ";
            if (!string.IsNullOrEmpty(StreetNumber) && StreetNumber != "0")
                address += StreetNumber + ", ";
            address += Locality;
            if (!string.IsNullOrEmpty(PostalCode) && PostalCode != "0")
                address += " " + PostalCode;
            if (!string.IsNullOrEmpty(Province))
                address += ", " + Province;
            if (Country != "Italy" && Country != "Italia")
                address += ", " + Country;
            return address;
            return string.IsNullOrEmpty(Locality)
                ? string.Empty
                :
                    (!string.IsNullOrEmpty(Route) ? Route + ", "
                        + (!string.IsNullOrEmpty(StreetNumber) && StreetNumber != "0" ? StreetNumber + ", " : string.Empty)
                        : string.Empty)
                    + Locality
                    + (!string.IsNullOrEmpty(PostalCode) && PostalCode != "0" ? " " + PostalCode : string.Empty)
                    + (!string.IsNullOrEmpty(Province) ? ", " + Province : string.Empty)
                    + (Country != "Italy" && Country != "Italia" ? ", " + Country : string.Empty);
        }

        public override string ToString()
        {
            return CompleteAddress();
        }
        public Address()
        {
            this.Country = "";
            this.Latitude = 0;
            this.Locality = "";
            this.Longitude = 0;
            this.PostalCode = "";
            this.Province = "";
            this.Region = "";
            this.Route = "";
            this.StreetNumber = "";
            this.TypedAddress = "";
        }

    }
}
